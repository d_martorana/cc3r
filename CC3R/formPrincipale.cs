﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using ARCA;
using System.Media;
using System.Diagnostics;

//using ARCA;

namespace CC3R
{
    public partial class frmPrincipale : Form
    {
        public RS232 mySerial = new RS232();
        public RS232 mySerialSx = new RS232();
        public RS232 mySerialDx = new RS232();
        public FUNCTIONS myFunc = new FUNCTIONS();
        public STRUCTURES myStruct = new STRUCTURES();
        //public ARCA.CC3R myCC3R = new ARCA.CC3R();
        public STRUCTURES.SCC3RProduct myCc3R = new STRUCTURES.SCC3RProduct();
        public STRUCTURES.SDConAnswer dConAnswer = new STRUCTURES.SDConAnswer();
        public STRUCTURES.SCCTalkAnswer ccTalkAnswer = new STRUCTURES.SCCTalkAnswer();
        public STRUCTURES.STest myTest = new STRUCTURES.STest();
        public STRUCTURES.SCC3RCoinHoppers hoppers = new STRUCTURES.SCC3RCoinHoppers();
        public STRUCTURES.SCCTalkAnswer ccTalkAnswerDx = new STRUCTURES.SCCTalkAnswer();
        public STRUCTURES.SCCTalkAnswer ccTalkAnswerSx = new STRUCTURES.SCCTalkAnswer();
        //public myArcaNameSpace.CC3R.SPrinterCmd printerCmd = new myArcaNameSpace.CC3R.SPrinterCmd();
        public int moduleAddress = 0;
        int[] sensorBit = new int[8];
        bool sensorCheckLoop = true;
        public INIFILE iniConfig;
        public string testResult = "";
        public string btnPressed = "";
        public string protocol = "";
        public string logFileName = Application.StartupPath + "\\CC3RTest.log";
        List<Label> myLabel ;
        public string coinDenomination = "EUR";
        public string goldenCoinSet = "388.00";

        public bool ControlInvokeRequired(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;
            return true;
        }
        
        delegate void SetTextCallBack(object sender, EventArgs e);
        public void ComDataSent(object sender, EventArgs e)//
        {
            if (ControlInvokeRequired(lstComScope, () => ComDataSent(sender, null))) return;
            lstComScope.Items.Add("SND:" + sender.ToString());
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
            switch (protocol)
            {
                case "cctalkDx":
                    ccTalkAnswerDx = new STRUCTURES.SCCTalkAnswer();
                    break;
                case "cctalkSx":
                    ccTalkAnswerSx = new STRUCTURES.SCCTalkAnswer();
                    break;
                default:
                    ccTalkAnswer = new STRUCTURES.SCCTalkAnswer();
                    break;
            }
        }

        public void ComDataReceived(object sender, EventArgs e)
        {
            if (ControlInvokeRequired(lstComScope, () => ComDataReceived(sender, null))) return;
            lstComScope.Items.Add("RCV:" + sender.ToString());
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
            dConAnswer.data = "";
            if (sender.ToString() != "NO ANSWER")
            {
                switch(protocol)
                {
                    case "cctalk":
                        ccTalkAnswer = myFunc.CcTalkAnswer(mySerial.commandAnswer);
                        break;
                    case "dcon":
                        dConAnswer = myFunc.DConAnswer(mySerial.commandAnswer);
                        break;
                    case "cctalkSx":
                        ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                        break;
                    case "cctalkDx":
                        ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                        break;
                }
            }
        }

        public frmPrincipale()
        {
            InitializeComponent();
            InitializeMyComponent();
        }

        public void InitializeMyComponent()
        {
            mySerial.ConfigCom("COM4", 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            mySerial.DataSent += new EventHandler(ComDataSent);
            mySerial.DataReceived += new EventHandler(ComDataReceived);
            mySerialDx.DataSent += new EventHandler(ComDataSent);
            mySerialDx.DataReceived += new EventHandler(ComDataReceived);
            mySerialSx.DataSent += new EventHandler(ComDataSent);
            mySerialSx.DataReceived += new EventHandler(ComDataReceived);
            myCc3R = new STRUCTURES.SCC3RProduct();
            iniConfig = new ARCA.INIFILE("setup.ini");
            //printerCmd = new myArcaNameSpace.CC3R.SPrinterCmd();
            myFunc.iniMessage= new INIFILE(iniConfig.GetValue("option", "messagesfile"));
            grpDcon.Location = new Point((scMain.Panel2.Width - grpDcon.Width) / 2, 150);
            grpSerial.Location = new Point((scMain.Panel2.Width - grpSerial.Width) / 2, 150);
            picImage.Location = new Point((scMain.Panel2.Width - picImage.Width) / 2, 150);
            picOk.Location = new Point((scMain.Panel2.Width - picOk.Width) / 4, 170);
            picKo.Location = new Point(((scMain.Panel2.Width - picKo.Width) / 4) * 3, 170);
            grpDataGrid.Location = new Point((scMain.Panel2.Width - grpDataGrid.Width) / 2, 150);
            picRetry.Location = picOk.Location;
            picExit.Location = picKo.Location;
            myCc3R.remoteIO.port = new STRUCTURES.SCC3RRemoteIOPort[8];
            myCc3R.remoteIO.port[0].description = "Cash till drawer RIGHT";
            myCc3R.remoteIO.port[0].status = "0";
            myCc3R.remoteIO.port[1].description = "Side shelf CT Coin rail door";
            myCc3R.remoteIO.port[1].status = "0";
            myCc3R.remoteIO.port[2].description = "Cash till drawer LEFT";
            myCc3R.remoteIO.port[2].status = "0";
            myCc3R.remoteIO.port[3].description = "Front DOOR";
            myCc3R.remoteIO.port[3].status = "0";
            myCc3R.remoteIO.port[4].description = "LEFT coin drawer";
            myCc3R.remoteIO.port[4].status = "0";
            myCc3R.remoteIO.port[5].description = "Side door UPPER";
            myCc3R.remoteIO.port[5].status = "0";
            myCc3R.remoteIO.port[6].description = "Side door LOWER";
            myCc3R.remoteIO.port[6].status = "0";
            myCc3R.remoteIO.port[7].description = "RIGHT coin drawer";
            myCc3R.remoteIO.port[7].status = "0";
            myTest.test = new STRUCTURES.SSingleTest[10];
            myTest.test[0].description = myFunc.iniMessage.GetValue("test","t0"); //Test di messa a terra e di isolamento elettrico
            myTest.test[1].description = myFunc.iniMessage.GetValue("test", "t1"); //Led indicazione accensione e dati
            myTest.test[2].description = myFunc.iniMessage.GetValue("test", "t2"); //Modulo serializzazione IO
            myTest.test[3].description = myFunc.iniMessage.GetValue("test", "t3"); //Calibrazione touchscreen
            myTest.test[4].description = myFunc.iniMessage.GetValue("test", "t4"); //Tastiera laterale
            myTest.test[5].description = myFunc.iniMessage.GetValue("test", "t5"); //Gettoniera di destra
            myTest.test[6].description = myFunc.iniMessage.GetValue("test", "t6"); //Gettoniera di sinistra
            myTest.test[7].description = myFunc.iniMessage.GetValue("test", "t7"); //Autoapprendimento CT Coin
            myTest.test[8].description = myFunc.iniMessage.GetValue("test", "t8"); //Stampante termica
            myTest.test[9].description = myFunc.iniMessage.GetValue("test", "t9"); //Prova funzionale sistema

            myTest.test[0].logID = myFunc.iniMessage.GetValue("logid", "id0");
            myTest.test[1].logID = myFunc.iniMessage.GetValue("logid", "id1");
            myTest.test[2].logID = myFunc.iniMessage.GetValue("logid", "id2");
            myTest.test[3].logID = myFunc.iniMessage.GetValue("logid", "id3");
            myTest.test[4].logID = myFunc.iniMessage.GetValue("logid", "id4");
            myTest.test[5].logID = myFunc.iniMessage.GetValue("logid", "id5");
            myTest.test[6].logID = myFunc.iniMessage.GetValue("logid", "id6");
            myTest.test[7].logID = myFunc.iniMessage.GetValue("logid", "id7");
            myTest.test[8].logID = myFunc.iniMessage.GetValue("logid", "id8");
            myTest.test[9].logID = myFunc.iniMessage.GetValue("logid", "id9");

            myLabel = new List<Label>();

            for (int i = 0; i< myTest.test.Length;i++)
            {
                myLabel.Add(new Label());
                scLeft.Panel2.Controls.Add(myLabel[i]);
                myLabel[i].Font = lblTestTitle.Font;
                myLabel[i].AutoSize = true;
                myLabel[i].Height = 13;
                myLabel[i].ForeColor = Color.White;
                myLabel[i].Text = myTest.test[i].description.ToUpper();
                myLabel[i].Location = new Point(0, (i * 20) + 20);
            }
        }

        private void frmPrincipale_Load(object sender, EventArgs e)
        {
        start:

            myCc3R.ctCoin.coin = new List<STRUCTURES.SCtCoinCoin>();
            myFunc.WriteMessage(lblTitle, 1); //CC3R TEST
            myFunc.WriteMessage(lblMessage, 2); //Inserire il codice del prodotto e premere invio.
            myFunc.userName = iniConfig.GetValue("login", "user").ToUpper();
            if (iniConfig.GetValue("option", "autologin") != "Y")
            {
                myFunc.LogInShow(myFunc.userName);
                if (iniConfig.GetValue("users", myFunc.userName) != myFunc.userPassword | iniConfig.GetValue("users", myFunc.userName).Length == 0)
                {
                    myFunc.WriteMessage(lblMessage, 301, myStruct.color.error, myFunc.userName);
                    myFunc.WaitingKey();
                    goto start;
                }
            }
            lblBottom.Text = "user:" + myFunc.userName;
            iniConfig.SetValue("login", "user", myFunc.userName);
            coinDenomination = iniConfig.GetValue("option", "CoinDenomination");
            goldenCoinSet = iniConfig.GetValue("GoldenCoinSet", coinDenomination);
            grpSerial.Visible = true;
            txtSerialNumber.Focus();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dgRemoteModule.Rows.Clear();
            dgRemoteModule.ColumnCount = 3;
            dgRemoteModule.Columns[0].HeaderText = "Module";
            dgRemoteModule.Columns[1].HeaderText = "Address";
            dgRemoteModule.Columns[2].HeaderText = "Firmware";

            string command = "";
            mySerial.OpenCom();

            for (moduleAddress = Convert.ToInt16(txtAddressStart.Text); moduleAddress <= Convert.ToInt16(txtAddressEnd.Text); moduleAddress++)
            {
                command = myFunc.DConCommand("$", moduleAddress, "M");
                mySerial.SendCommand(command, 10, 100);
                if (dConAnswer.data != "")
                {
                    myCc3R.remoteIO.module = dConAnswer.data;
                    myCc3R.remoteIO.address = dConAnswer.moduleAddress;
                    command = myFunc.DConCommand("$", moduleAddress, "F");
                    mySerial.SendCommand(command, 6, 100);
                    myCc3R.remoteIO.firmware.release = dConAnswer.data;
                    command = myFunc.DConCommand("$", moduleAddress, "P");
                    mySerial.SendCommand(command, 10, 1000);
                    myCc3R.remoteIO.commProt = dConAnswer.data;
                    dgRemoteModule.Rows.Add(myCc3R.remoteIO.module, myCc3R.remoteIO.address, myCc3R.remoteIO.firmware);
                    break;
                }
            }

            mySerial.CloseCom();
            dgRemoteModule.Width = dgRemoteModule.Columns[0].Width + dgRemoteModule.Columns[1].Width + dgRemoteModule.Columns[2].Width + 5;

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            mySerial.OpenCom();
            string command = myFunc.DConCommand(txtSend.Text);
            mySerial.SendCommand(command, 10, 1000);
            lblAnswer.Text = dConAnswer.data;
            mySerial.CloseCom();
        }

        private void SensorResult(int bit)
        {
            if (ControlInvokeRequired(grpDcon, () => SensorResult(bit))) return;
            PictureBox pic = new PictureBox();
            switch (bit)
            {
                case 0:
                    pic = picBit7;
                    break;
                case 1:
                    pic = picBit6;
                    break;
                case 2:
                    pic = picBit5;
                    break;
                case 3:
                    pic = picBit4;
                    break;
                case 4:
                    pic = picBit3;
                    break;
                case 5:
                    pic = picBit2;
                    break;
                case 6:
                    pic = picBit1;
                    break;
                case 7:
                    pic = picBit0;
                    break;
            }
            if (sensorBit[bit] == 1)
            {
                pic.Image = Image.FromFile("greenLight.png");
            }
            else
            {
                pic.Image = Image.FromFile("redLight.png");
            }

            grpDcon.Refresh();
        }
        private void btnCheckIo_Click(object sender, EventArgs e)
        {
            //frmPrincipale thr1 = new frmPrincipale();
            Thread tid1 = new Thread(new ThreadStart(CheckIO));
            tid1.Start();

        }

        public void CheckIO()
        {
            mySerial.OpenCom();
            string command = myFunc.DConCommand("@01");
            do
            {
                mySerial.SendCommand(command, 10, 100);
                for (int i = 0; i <= 7; i++)
                {
                    sensorBit[i] = Convert.ToInt16(myFunc.Hex2Bin(dConAnswer.data).Substring(i, 1));
                    SensorResult(i);
                }
                myFunc.WaitingTime(100);
            } while (sensorCheckLoop == true);
            mySerial.CloseCom();
            sensorCheckLoop = true;

            picBit0.Image = Image.FromFile("blackLight.png");
            picBit1.Image = Image.FromFile("blackLight.png");
            picBit2.Image = Image.FromFile("blackLight.png");
            picBit3.Image = Image.FromFile("blackLight.png");
            picBit4.Image = Image.FromFile("blackLight.png");
            picBit5.Image = Image.FromFile("blackLight.png");
            picBit6.Image = Image.FromFile("blackLight.png");
            picBit7.Image = Image.FromFile("blackLight.png");

        }
        private void btnCheckStop_Click(object sender, EventArgs e)
        {
            sensorCheckLoop = false;
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            grpDcon.Visible = !grpDcon.Visible;
        }

        private void pictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            picN0.BackColor = Color.White;
        }

        private void NumberClick(object sender, EventArgs e)
        {
            myFunc.keyCodePressed = 0;
            PictureBox pic = (PictureBox)sender;
            if (txtSerialNumber.Text.Length < 12)
            {
                switch (pic.Name)
                {
                    case "picN0":
                        txtSerialNumber.Text += 0;
                        break;
                    case "picN1":
                        txtSerialNumber.Text += 1;
                        break;
                    case "picN2":
                        txtSerialNumber.Text += 2;
                        break;
                    case "picN3":
                        txtSerialNumber.Text += 3;
                        break;
                    case "picN4":
                        txtSerialNumber.Text += 4;
                        break;
                    case "picN5":
                        txtSerialNumber.Text += 5;
                        break;
                    case "picN6":
                        txtSerialNumber.Text += 6;
                        break;
                    case "picN7":
                        txtSerialNumber.Text += 7;
                        break;
                    case "picN8":
                        txtSerialNumber.Text += 8;
                        break;
                    case "picN9":
                        txtSerialNumber.Text += 9;
                        break;
                    default:
                        break;
                }
            }
            if (pic.Name == "picTrash")
            {
                txtSerialNumber.Tag = "ProductCode";
                txtSerialNumber.Text = "";
                myFunc.WriteMessage(lblMessage, 2); //Inserire il codice del prodotto e premere invio.
            }
            if (pic.Name == "picEnter" & txtSerialNumber.Text.Length > 4)
            {
                KeyEventArgs a = new KeyEventArgs(Keys.Enter);
                myFunc.keyCodePressed = 13;
                KeyPreddedOnForm(sender, a);

            }
            txtSerialNumber.SelectionStart = txtSerialNumber.Text.Length;
            txtSerialNumber.SelectionLength = 0;
        }

        string TouchCalibration(int testID)
        {
            testResult = "OK";
            myLabel[testID].ForeColor = Color.LightBlue;
            myFunc.WriteMessage(lblTitle, 19); //ELO TOUCH SCREEN
            myFunc.WriteMessage(lblMessage, 18); //Toccare lo schermo per avviare la calibrazione del touch screen
            myFunc.WaitingKey();
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.FileName = "C:\\Program files\\Elo touch Solutions\\EloConfig.exe";
            p.StartInfo.Arguments = "/align";
            p.StartInfo.RedirectStandardOutput = false;
            p.Start();
            p.WaitForExit();
            if (TestYesNo(testID, 19, 20) != "OK") { testResult = "KO"; } //ELO TOUCH SCREEN - La calibrazione del touch screen e' andata a buon fine?
            return testResult;
        }

        STRUCTURES.SCC3RCtCoinDisplay ReadCtCoinDisplay()
        {
            mySerial.SendCommand(myFunc.CtCoinGetDisplayContent());
            STRUCTURES.SCC3RCtCoinDisplay lcd = new STRUCTURES.SCC3RCtCoinDisplay();
            lcd.menu = "ERROR";
            if (mySerial.commandAnswer.Length != 96)
                return lcd;
            string lcdText = myFunc.Hex2Ascii(mySerial.commandAnswer.Substring(10, mySerial.commandAnswer.Length - 16));

            // HOME / SETUP / PROGRAMMING / LIFE

            if (lcdText.Substring(0, 4) == "Coin")
            {
                lcd.menu = "LIFE";
                lcd.upLeft = lcdText.Substring(0, 7).Trim();
                lcd.upRight = lcdText.Substring(14,6).Trim();
                lcd.downLeft = lcdText.Substring(20,0).Trim();
                lcd.downRight= lcdText.Substring(33,7).Trim();
            }
            if (lcdText.Substring(0, 5) == "Total")
            {
                lcd.menu = "LIFE";
                lcd.upLeft = lcdText.Substring(0, 19).Trim();
                lcd.upRight = lcdText.Substring(19,0).Trim();
                lcd.downLeft = lcdText.Substring(20, 0).Trim();
                lcd.downRight = lcdText.Substring(33,7).Trim();
            }

            if (lcdText.Substring(0, 5) == "Setup")
            {
                lcd.menu = "SETUP";
                lcd.upLeft = lcdText.Substring(0, 5).Trim();
                lcd.upRight = lcdText.Substring(14,6).Trim();
                lcd.downLeft = lcdText.Substring(20, 0).Trim();
                lcd.downRight = lcdText.Substring(33,7).Trim();
            }

            if (lcdText.Substring(2, 1) == ":" || lcdText.Substring(0,4) == "    ")
            {
                lcd.menu = "HOME";
                lcd.upLeft = lcdText.Substring(0, 5).Trim();
                lcd.upRight = lcdText.Substring(6, 0).Trim();
                lcd.downLeft = lcdText.Substring(20, 12).Trim();
                lcd.downRight = lcdText.Substring(33,7).Trim();
            }

            if (lcdText.Substring(0,11)=="Programming")
            {
                lcd.menu = "PROGRAMMING";
                lcd.upLeft = lcdText.Substring(0, 11).Trim();
                lcd.upRight = lcdText.Substring(12, 0).Trim();
                lcd.downLeft = lcdText.Substring(20, 13).Trim();
                lcd.downRight = lcdText.Substring(33, 7).Trim();
            }
            return lcd;
        }

        string CtCoinLifeCounter()
        {
            string result = "PASS";
            mySerial.OpenCom();
            mySerial.SendCommand(myFunc.CtCoinConnect());
            int pressedTimes = 0;
        menuSetup:
            mySerial.SendCommand(myFunc.CtCoinKeyPress("SETUP"));
            if ((myCc3R.ctCoin.lcd = ReadCtCoinDisplay()).menu != "SETUP")
            {
                pressedTimes++;
                mySerial.SendCommand(myFunc.CtCoinKeyPress("PROGRAMMING"));
                if (pressedTimes <= 5)
                    goto menuSetup;
                result = "FAIL";
                goto end;
            }
            mySerial.SendCommand(myFunc.CtCoinKeyPress("2"));
            mySerial.SendCommand(myFunc.CtCoinKeyPress("5"));
            mySerial.SendCommand(myFunc.CtCoinKeyPress("ENTER"));
            if ((myCc3R.ctCoin.lcd = ReadCtCoinDisplay()).menu != "LIFE")
            {
                result = "FAIL";
                goto end;
            }
            myCc3R.ctCoin.life = Convert.ToInt16(myCc3R.ctCoin.lcd.downRight);
            myFunc.LogWrite(logFileName, ",450" + myCc3R.ctCoin.life);
            myFunc.WriteMessage(lblMessage, 42, Color.White, myCc3R.ctCoin.life); //Il totale di monete processate e' {0}.
            myFunc.WaitingTime(4000);
            dgInformationTable.Rows.Clear();
            dgInformationTable.ColumnCount = 3;
            dgInformationTable.Columns[0].HeaderText = "DESCRIPTION";
            dgInformationTable.Columns[1].HeaderText = "VALUE";
            dgInformationTable.Columns[2].HeaderText = "LIFE";
            grpDataGrid.Visible = true;
            dgInformationTable.Visible = true;
            dgInformationTable.Rows.Add("TOTAL","", myCc3R.ctCoin.life);
            bool lastCoin = false;
            do
            {
                myCc3R.ctCoin.lcd = ReadCtCoinDisplay();
                mySerial.SendCommand(myFunc.CtCoinKeyPress(">"));
                if (myCc3R.ctCoin.lcd.menu == "LIFE")
                {
                    STRUCTURES.SCtCoinCoin coin = new STRUCTURES.SCtCoinCoin();
                    coin.bank = Convert.ToInt16(myCc3R.ctCoin.lcd.upLeft.Substring(5, 2)).ToString();
                    coin.life = Convert.ToInt16(myCc3R.ctCoin.lcd.downRight);
                    coin.value = myCc3R.ctCoin.lcd.upRight;
                    if (myCc3R.ctCoin.coin == null)
                        myCc3R.ctCoin.coin = new List<STRUCTURES.SCtCoinCoin>();
                    for (int i = 0; i < myCc3R.ctCoin.coin.Count; i++)
                    {
                        if (myCc3R.ctCoin.coin[i].bank == coin.bank)
                        {
                            lastCoin = true;
                            break;
                        }
                    }
                    if (lastCoin == false)
                    {
                        myCc3R.ctCoin.coin.Add(coin);
                        dgInformationTable.Rows.Add("Coin " + coin.bank, coin.value, coin.life);
                        myFunc.WriteMessage(lblMessage, 43, Color.White, coin.value, coin.life); //Il totale delle monete da {0} processate e' {1}.
                        myFunc.LogWrite(logFileName, ",45" + coin.bank + coin.life + ",06" + coin.bank + coin.value);
                    }
                }
            } while (lastCoin == false);


        end:
            do
            {
                mySerial.SendCommand(myFunc.CtCoinKeyPress("PROGRAM"));
                myCc3R.ctCoin.lcd = ReadCtCoinDisplay();
            } while (myCc3R.ctCoin.lcd.menu != "HOME");
            grpDataGrid.Visible = false;
            mySerial.SendCommand(myFunc.CtCoinDisconnect());
            mySerial.CloseCom();

            return result;
            
        }

        string CtCoinAutoLearnig(int testId)
        {
            startTest:
            testResult = "FAIL";
            mySerial.CloseCom();
            myLabel[testId].ForeColor = Color.LightBlue;
            mySerial.ConfigCom(iniConfig.GetValue("CTCoin", "PortName"));
            //estrarre il ctcoin
            //montare il 'learning tray'
            //verifica conteggio > 5000
            //verificare il deflettore con l'attrezzo
            //verificare che il settaggio autolearning sia su ON
            //'depositare' il golden test set, una denominazione alla volta

            myFunc.WriteMessage(lblTitle, 32); //CT COIN TEST
            myFunc.WriteMessage(lblMessage, 33); //Estrarre il vassoio del CT Coin e montare l'attrezzo per l'autoapprendimento. Premere un tasto per continuare
            myFunc.WaitingKey();

            CtCoinLifeCounter();
            if (myCc3R.ctCoin.life < 5000)
            {
                return testResult;
            }

            if ((testResult = TestYesNo(testId,39,34))!="OK") //La verifica dei deflettori con le dime e' andata a buon fine?
                return testResult;

            mySerial.OpenCom();
            mySerial.SendCommand(myFunc.CtCoinConnect());
            coinCounting:
            myFunc.WriteMessage(lblMessage, 46); //Inserire le monete del golden coin set e toccare lo schermo per avviare il conteggio.
            myFunc.WaitingKey();
            mySerial.SendCommand(myFunc.CtCoinKeyPress("CLEAR"));
            mySerial.SendCommand(myFunc.CtCoinKeyPress("START"));
            waitEnd:
            myFunc.WriteMessage(lblMessage, 47); //Conteggio in corso, toccare lo schermo a monete terminate
            myFunc.WaitingKey();
            STRUCTURES.SCC3RCtCoinDisplay lcd = ReadCtCoinDisplay();
            if (lcd.downRight != goldenCoinSet) //388.00 = EUR KIT - xxx.xx = USD KIT
            {
                if (lcd.downRight == "")
                    goto waitEnd;
                string button = "";
                RetryOrExit(32, 311, ref button, lcd.downRight, goldenCoinSet); //Il risultato del conteggio e' {0} invece di {1}, ripetere o uscire?
                if (button == "ESC")
                    return "KO";
                goto coinCounting;
            }

        //mySerial.CloseCom();

        //entrare in modalità autolearning
        // setup - 4 - enter - 1 -
        //mySerial.OpenCom();
        //mySerial.SendCommand(myFunc.CtCoinConnect());

        autoLearningStart:
            do
            {
                mySerial.SendCommand(myFunc.CtCoinKeyPress("PROGRAM"));
                myCc3R.ctCoin.lcd = ReadCtCoinDisplay();
            } while (myCc3R.ctCoin.lcd.menu != "HOME");

            mySerial.SendCommand(myFunc.CtCoinKeyPress("SETUP"));
            mySerial.SendCommand(myFunc.CtCoinKeyPress("4"));
            mySerial.SendCommand(myFunc.CtCoinKeyPress("ENTER"));
            myFunc.WriteMessage(lblMessage, 35); //Inserire le monete per eseguire l'autoapprendimento. Un taglio alla volta dopo averlo selezionato. Toccare lo schermo quando terminato, o ESC per uscire
            picImage.Image = Properties.Resources.calib;
            picImage.Visible = true;
            myFunc.WaitingKey();
            picImage.Visible = false;
            testResult = TestYesNo(7, 39, 40); //CT COIN AUTOLEANING - L'autotaratura e' avvenuta correttamente?
            if (testResult=="KO")
            {
                string button = "";
                RetryOrExit(32, 312, ref button, lcd.downRight); //Autolearning non andato a buon fine. Ripetere il test o uscire?
                if (button == "ESC")
                    return "KO";
                goto autoLearningStart;
            }

            //coinCounting2:
            //do
            //{
            //    mySerial.SendCommand(myFunc.CtCoinKeyPress("PROGRAM"));
            //    myCc3R.ctCoin.lcd = ReadCtCoinDisplay();
            //} while (myCc3R.ctCoin.lcd.menu != "HOME");
            //myFunc.WriteMessage(lblMessage, 46); //Inserire tutte le monete del golden coin set e toccare lo schermo per avviare il conteggio.
            //myFunc.WaitingKey();
            //mySerial.SendCommand(myFunc.CtCoinKeyPress("CLEAR"));
            //mySerial.SendCommand(myFunc.CtCoinKeyPress("START"));
            //waitEnd2:
            //myFunc.WriteMessage(lblMessage, 47); //Toccare lo schermo a fine conteggio
            //myFunc.WaitingKey();
            //// leggi display, totale = 388 euro
            //lcd = ReadCtCoinDisplay();
            //if (lcd.downRight != "388.00" && lcd.downRight != "388.00") //388.00 = EUR KIT - xxx.xx = USD KIT
            //{
            //    if (lcd.downRight == "")
            //        goto waitEnd2;
            //    string button = "";
            //    RetryOrExit(32, 311, ref button, lcd.downRight); //Sono stati conteggiati {0} che non è il valore atteso, ripetere o uscire?
            //    if (button == "ESC")
            //        return "KO";
            //    goto coinCounting2;
            //}

            myFunc.WriteMessage(lblMessage, 48);//Rimuovere l'attrezzo per l'autoapprendimento, chiudere il vassoio e lo sportello. Toccare lo schermo per continuare
            myFunc.WaitingKey();
            mySerial.SendCommand(myFunc.CtCoinDisconnect());
            mySerial.CloseCom();
            testResult = "OK";
            return testResult;
        }

        void CC3RTEST()
        {
            testResult = "OK";
            string printerString = "CC3R TEST REPORT";
            foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.NetworkInterfaceType == System.Net.NetworkInformation.NetworkInterfaceType.Ethernet && nic.OperationalStatus==System.Net.NetworkInformation.OperationalStatus.Up && nic.Description.Substring(0,7)!="Virtual")
                {
                    myCc3R.MACAddress = nic.GetPhysicalAddress().ToString();
                }
            }
            

        testStart:
            string log = System.Environment.NewLine + "001" + myCc3R.productCode + ",004" + myCc3R.productSerialNumber + ",055" + myFunc.userName +
                ",005" + Application.ProductVersion + ",006" + System.Environment.MachineName + ",008" + myCc3R.MACAddress +
                ",300" + DateTime.Now.ToShortDateString() + ",301" + DateTime.Now.ToShortTimeString();
            printerString += "\n\nDATE: " + DateTime.Now.ToShortDateString();
            printerString += "\nTIME: " + DateTime.Now.ToShortTimeString();
            printerString += "\n\nPRODUCT CODE : " + myCc3R.productCode;
            printerString += "\nSERIAL NUMBER: " + myCc3R.productSerialNumber;
            printerString += "\n----------------------------";
            myFunc.LogWrite(logFileName, log);

            if ((testResult = TestYesNo(testIndex: 0, titleNumber: 5, messageNumber: 4)) != "OK") { goto endTest; } //HIPOT e GC TEST - Le prove di messa a terra e di isolamento termico sono state effettuate correttamente?
            printerString += "\nHIPOT & GC TEST  : PASS";

            if ((testResult = TestYesNo(testIndex: 1, titleNumber: 6, messageNumber: 7)) != "OK") { goto endTest; } //HAPPY LIGHTS - I led sono accesi correttamente? (switch on/off, 12V, 24V e IO converter)
            printerString += "\nHAPPY LIGHT      : PASS";

            //goto autolearningTest;

        ioTest:
            if ((testResult= IOConverterTest(testID: 2)) != "OK")
            {
                RetryOrExit(8, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto ioTest;
            }
            printerString += "\nI/O CONVERTER    : PASS";

        touchTest:
            if ((testResult= TouchCalibration(testID: 3)) != "OK")
            {
                RetryOrExit(19, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto touchTest;
            }
            printerString += "\nTOUCH CALIBRATION: PASS";

        keypadTest:
            if ((testResult= SideKeypadTest(testId: 4)) != "OK")
            {
                RetryOrExit(21, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto keypadTest;
            }
            printerString += "\nSIDE KEYPAD      : PASS";
            if (TestYesNo(4, 21, 22, iniConfig.GetValue("SideKeypad", "FWName")) != "OK")//Il display della tastiera laterale deve riportare la scritta "{0}". E' corretto?
            {
                RetryOrExit(21, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto keypadTest;
            }

        dxHopperTest:
            if ((testResult = CoinHopperDx(5)) != "OK")
            {
                RetryOrExit(27, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto dxHopperTest;
            }
            printerString += "\nCOIN HOPPER DX   : PASS";

        sxHopperTest:
            if ((testResult = CoinHopperSx(6)) != "OK")
            {
                RetryOrExit(27, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto sxHopperTest;
            }
            printerString += "\nCOIN HOPPER SX   : PASS";

        autolearningTest:
            if ((testResult = CtCoinAutoLearnig(testId: 7)) != "OK")
            {
                RetryOrExit(32, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto autolearningTest;
            }
            printerString += "\nCTCOIN           : PASS";

        ctCoinDeposit:
            if ((testResult = CtCoinDeposit()) != "OK")
            {
                RetryOrExit(32, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto ctCoinDeposit;
            }

        //svuotare gli hopper
        emptyHopper:
            if ((testResult = EmptyHopper("")) != "OK")
            {
                RetryOrExit(32, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto emptyHopper;
            }


        //riconteggio monete. Utilizzare i contatori degli hopper o ripetere conteggio con il ctcoin ed il vassoio dell'autolearning

        printer:
            printerString += "\nTHERMAL PRINTER  : PASS";
            if ((testResult = PrinterTest(printerString, 8))!= "OK")
            {
                RetryOrExit(38, 310, ref btnPressed); //TEST FALLITO! Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FAIL";
                    goto endTest;
                }
                goto printer;
            }

        endTest:
            if (testResult == "OK" || testResult == "PASS")
            {
                testResult = "PASS";
                myFunc.WriteMessage(lblTitle, 1);// CC3R Test
                myFunc.WriteMessage(lblMessage, 24); //Test eseguito correttamente. Toccare lo schermo per uscire.
            }
            else
            {
                testResult = "KO";
                myFunc.WriteMessage(lblTitle, 1);// CC3R Test
                myFunc.WriteMessage(lblMessage, 307, myStruct.color.error); //TEST FALLITO! toccare lo schermo per uscire.
            }
            myFunc.LogWrite(logFileName, ",305" + DateTime.Now.ToShortDateString() + ",306" + DateTime.Now.ToShortTimeString() + ",302" + testResult);
            HideControls(scMain.Panel2);
            myFunc.WaitingKey();
            Application.Exit();
        }

        string EmptyHopper(string side = "")
        {
            bool emptyDx = true;
            bool emptySx = true;
            testResult = "OK";
            myFunc.WriteMessage(lblMessage, 52); //Svuotamento di tutti gli hopper in corso
            if (side == "" || side == "dx")
            {
                emptyDx = false;
                mySerialDx.ConfigCom(iniConfig.GetValue("CoinHopperDx", "PortName"));
                mySerialDx.OpenCom();
                protocol = "cctalkDx";
                mySerialDx.SendCommand(myFunc.CcTalkAddressPoll());
                hoppers.hoppersDx = new STRUCTURES.SCC3RHopper[ccTalkAnswerDx.answer.Length / 2];
                for (int i = 0; i < ccTalkAnswerDx.answer.Length / 2; i++)
                {
                    hoppers.hoppersDx[i].address = Convert.ToInt16(ccTalkAnswerDx.answer.Substring(i * 2, 2));
                }
            }

            if (side == "" || side == "sx")
            {
                emptyDx = false;
                mySerialSx.ConfigCom(iniConfig.GetValue("CoinHopperSx", "PortName"));
                mySerialSx.OpenCom();
                protocol = "cctalkSx";
                mySerialSx.SendCommand(myFunc.CcTalkAddressPoll());
                hoppers.hoppersSx = new STRUCTURES.SCC3RHopper[ccTalkAnswerSx.answer.Length / 2];
                for (int i = 0; i < ccTalkAnswerSx.answer.Length / 2; i++)
                {
                    hoppers.hoppersSx[i].address = Convert.ToInt16(ccTalkAnswerSx.answer.Substring(i * 2, 2));
                }
            }

            myFunc.WaitingTime(1500);

            int numberCoin = 0;
            string coinQty = "";
            string serial = "";

            if (side == "" || side == "dx")
            {
                protocol = "cctalkDx";
                for (int id = 3; id <= 6; id++)
                {
                    mySerialDx.SendCommand(myFunc.CcTalkGetSerialNumber(id));
                    ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                    serial = ccTalkAnswerDx.data;
                    mySerialDx.SendCommand(myFunc.CcTalkGetDiameters(id));
                    ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                    numberCoin = Convert.ToInt16(ccTalkAnswerDx.data.Length / 4);
                    coinQty = "";
                    for (int i = 1; i <= numberCoin; i++)
                        coinQty += "00FF";
                    mySerialDx.SendCommand(myFunc.CcTalkCommand(id, 1, "FE")); // Single Poll
                    mySerialDx.SendCommand(myFunc.CcTalkCommand(id, 1, "A4", "A5")); // Enable Hopper
                    mySerialDx.SendCommand(myFunc.CcTalkCommand(id, 1, "20", serial + coinQty)); // Multiple Payment
                }
            }

            if (side == "" || side == "sx")
            {
                protocol = "cctalkSx";
                for (int id = 3; id <= 6; id++)
                {
                    mySerialSx.SendCommand(myFunc.CcTalkGetSerialNumber(id));
                    ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                    serial = ccTalkAnswerSx.data;
                    mySerialSx.SendCommand(myFunc.CcTalkGetDiameters(id));
                    ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                    numberCoin = Convert.ToInt16(ccTalkAnswerSx.data.Length / 4);
                    coinQty = "";
                    for (int i = 1; i <= numberCoin; i++)
                        coinQty += "00FF";
                    mySerialSx.SendCommand(myFunc.CcTalkCommand(id, 1, "FE")); // Single Poll
                    mySerialSx.SendCommand(myFunc.CcTalkCommand(id, 1, "A4", "A5")); // Enable Hopper
                    mySerialSx.SendCommand(myFunc.CcTalkCommand(id, 1, "20", serial + coinQty)); // Multiple Payment
                }
            }

            

            while (emptyDx == false || emptySx == false)
            {
                if (emptyDx==false)
                {
                    //A6
                    emptyDx = true;
                    for (int i = 0; i < hoppers.hoppersDx.Count();i++)
                    {
                        if (hoppers.hoppersDx[i].status != "paid")
                        {
                            emptyDx = false;
                            mySerialDx.SendCommand(myFunc.CcTalkCommand(hoppers.hoppersDx[i].address, 1, "A6"));
                            ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                            hoppers.hoppersDx[i].status = "paying";
                            if (ccTalkAnswerDx.data.Substring(2, 2) == "00")
                            {
                                hoppers.hoppersDx[i].status = "paid";
                                //hoppers.hoppersDx[i].paidCoin = ccTalkAnswerDx.data.Substring(4, 2);
                            }
                        }
                    }
                }

                if (emptySx==false)
                {
                    emptySx = true;
                    for (int i = 0; i < hoppers.hoppersSx.Count(); i++)
                    {
                        if (hoppers.hoppersSx[i].status != "paid")
                        {
                            emptySx = false;
                            mySerialSx.SendCommand(myFunc.CcTalkCommand(hoppers.hoppersSx[i].address, 1, "A6"));
                            ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                            hoppers.hoppersSx[i].status = "paying";
                            if (ccTalkAnswerSx.data.Substring(2, 2) == "00")
                            {
                                hoppers.hoppersSx[i].status = "paid";
                                //hoppers.hoppersSx[i].paidCoin = ccTalkAnswerSx.data.Substring(4, 2);
                            }
                        }
                    }
                }
            }

            if (side == "" || side == "dx")
                mySerialDx.CloseCom();
            if (side == "" || side == "sx")
                mySerialSx.CloseCom();
            
            return testResult;
        }

        string PrinterTest(string data, int testId)
        {
            testResult = "OK";
            myLabel[testId].ForeColor = Color.LightBlue;
            string command = "";
            mySerial.CloseCom();
            myFunc.WriteMessage(lblMessage, 37);//Inserire il rotolo della carta nella stampante. Premere un tasto per continuare
            myFunc.WaitingKey();
            mySerial.ConfigCom(iniConfig.GetValue("Printer", "portName"));
            command = Constants.printerSelectFont + "01";
            mySerial.SendCommand(command, 1, 250);
            command = Constants.printerLineFeed;
            command = myFunc.Ascii2Hex(data) + command;
            mySerial.SendCommand(command, 1, 250);
            command = Constants.printerFeedPaper + "FF";
            mySerial.SendCommand(command, 1, 250);
            command = Constants.printerPartialCut;
            mySerial.SendCommand(command, 1, 250);
            if (TestYesNo(testIndex: 8, titleNumber: 38, messageNumber: 36) != "OK") { return "FAIL"; } //PRINTER - La stampante ha stampato gli esiti dei test ed il foglio e' stato pretagliato correttamente?
            return testResult;
        }

        string CtCoinDeposit()
        {
            testResult = "OK";
            myFunc.WriteMessage(lblMessage, 49);//Verificare che lo sportello del CtCoin sia correttamente chiuso, inserise le monete nella vasca e toccare lo schermo avviare il deposito
            myFunc.WaitingKey();

            mySerial.ConfigCom(iniConfig.GetValue("CTCoin", "portName"));
            mySerial.OpenCom();
            mySerial.SendCommand(myFunc.CtCoinConnect());
            do
            {
                mySerial.SendCommand(myFunc.CtCoinKeyPress("PROGRAM"));
                myCc3R.ctCoin.lcd = ReadCtCoinDisplay();
            } while (myCc3R.ctCoin.lcd.menu != "HOME");

            mySerial.SendCommand(myFunc.CtCoinKeyPress("START"));
            myFunc.WriteMessage(lblMessage, 51); //Toccare lo schermo a deposito finito
            myFunc.WaitingKey();

            mySerial.SendCommand(myFunc.CtCoinDisconnect());
            mySerial.CloseCom();
            return testResult;
        }

        void HideControls(Panel panelName)
        {
            foreach (Control ctrl in panelName.Controls)
            {
                if (ctrl is GroupBox | ctrl is PictureBox)
                {
                    ctrl.Visible = false;
                }
            }
        }
        string SideKeypadTest(int testId)
        {
            startTest:
            testResult = "KO";
            mySerial.CloseCom();
            myLabel[testId].ForeColor = Color.LightBlue;
            myFunc.WriteMessage(lblTitle, 21); //SIDE KEYPAD
            myFunc.WriteMessage(lblMessage, 25); //Verifica fw attualmente installato in corso.
            mySerial.ConfigCom(iniConfig.GetValue("SideKeypad", "PortName"));
            mySerial.OpenCom();
            mySerial.SendCommand(myFunc.KeyPadReset());
            mySerial.CloseCom();
            if (mySerial.commandAnswer == "NO ANSWER" | mySerial.commandAnswer.Length==0)
                goto endTest;
            myCc3R.keyPad.fw = myFunc.Hex2Ascii(mySerial.commandAnswer.Substring(12, mySerial.commandAnswer.Length - 16));
            myFunc.LogWrite(logFileName, ",101" + myCc3R.keyPad.fw);
            if (myCc3R.keyPad.fw != iniConfig.GetValue("SideKeypad", "FWVersion"))
            {
                RetryOrExit(303, 308, ref btnPressed, iniConfig.GetValue("SideKeypad", "FWVersion"), myCc3R.keyPad.fw); //Firmware keypad laterale installato non corretto. Atteso {0} ma riscontrato {1}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FWKO";
                    goto endTest;
                }
                goto startTest;
            }
            testResult = "OK";

            // Aggiungere pressione tasto keypad laterale
            endTest:
            if (testResult=="OK")
                myLabel[testId].ForeColor = Color.LightGreen;
            else
                myLabel[testId].ForeColor = Color.Orange;

            return testResult;
        }


        string CoinHopperDx(int testId)
        {
            protocol = "cctalk";
            testResult = "KO";
            mySerial.CloseCom();
            myLabel[testId].ForeColor = Color.LightBlue;
            try
            {
                mySerial.ConfigCom(iniConfig.GetValue("CoinHopperDx", "PortName"));
                myFunc.WriteMessage(lblMessage, 28); //Aprire la porta ed estrarre il vassoio degli hopper di destra. Toccare lo schermo per continuare.
                myFunc.WaitingKey();
            //ricerca hopper collegati
            startTest:
                myFunc.WriteMessage(lblTitle, 27);//HOPPER
                myFunc.WriteMessage(lblMessage, 26);//Ricerca degli Hopper collegati
                dgInformationTable.Rows.Clear();
                dgInformationTable.Columns.Clear();
                mySerial.SendCommand(myFunc.CcTalkAddressPoll());
                myCc3R.coinHoppers.hoppersDx = new STRUCTURES.SCC3RHopper[ccTalkAnswer.answer.Length / 2];
                if (mySerial.commandAnswer.Length == 0 | mySerial.commandAnswer == "NO ANSWER")
                    goto endTest;

                for (int i = 0; i < ccTalkAnswer.answer.Length / 2; i++)
                {
                    myCc3R.coinHoppers.hoppersDx[i].address = Convert.ToInt16(ccTalkAnswer.answer.Substring(i * 2, 2));
                }

                if (myCc3R.coinHoppers.hoppersDx.Length == 0)
                {
                    dgInformationTable.Visible = false;
                    RetryOrExit(303, 309, ref btnPressed); //Nessun hopper trovato. Verifica la connessione. Ripetere il test o uscire?
                    if (btnPressed == "ESC")
                    {
                        testResult = "NOHOPPER";
                        goto endTest;
                    }
                    dgInformationTable.Visible = true;
                    goto startTest;
                }
                myFunc.WaitingTime(1500);
                dgInformationTable.ColumnCount = 7;
                dgInformationTable.Columns[0].HeaderText = "Side";
                dgInformationTable.Columns[1].HeaderText = "Address";
                dgInformationTable.Columns[2].HeaderText = "Category";
                dgInformationTable.Columns[3].HeaderText = "Manuf. ID";
                dgInformationTable.Columns[4].HeaderText = "Prod. Code";
                dgInformationTable.Columns[5].HeaderText = "Serial Number";
                dgInformationTable.Columns[6].HeaderText = "Diameters";
                dgInformationTable.Columns[1].MinimumWidth = 80;
                grpDataGrid.Visible = true;
                for (int i = 0; i < myCc3R.coinHoppers.hoppersDx.Length; i++)
                {
                    myFunc.WriteMessage(lblMessage, 29); //Richiesta informazioni sugli hopper collegati
                    mySerial.SendCommand(myFunc.CcTalkGetEquipCat(myCc3R.coinHoppers.hoppersDx[i].address));
                    myCc3R.coinHoppers.hoppersDx[i].equipCategory = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkGetManufID(myCc3R.coinHoppers.hoppersDx[i].address));
                    myCc3R.coinHoppers.hoppersDx[i].manufacturerId = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkGetProdCode(myCc3R.coinHoppers.hoppersDx[i].address));
                    myCc3R.coinHoppers.hoppersDx[i].productCode = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkGetSerialNumber(myCc3R.coinHoppers.hoppersDx[i].address));
                    myCc3R.coinHoppers.hoppersDx[i].serialNumber = myFunc.Hex2Dbl(ccTalkAnswer.answer.Substring(12, 2) + ccTalkAnswer.answer.Substring(10, 2) + ccTalkAnswer.answer.Substring(8, 2)).ToString();

                    mySerial.SendCommand(myFunc.CcTalkGetDiameters(myCc3R.coinHoppers.hoppersDx[i].address));
                    myCc3R.coinHoppers.hoppersDx[i].diameter = new int[((ccTalkAnswer.answer.Length - 8) / 4)];
                    string diameters = "";
                    string diametersLog = "";
                    for (int x = 0; x < myCc3R.coinHoppers.hoppersDx[i].diameter.Length; x++)
                    {
                        myCc3R.coinHoppers.hoppersDx[i].diameter[x] = myFunc.Hex2Dec(ccTalkAnswer.answer.Substring(8 + (x * 4), 4));
                        diameters += (x + 1).ToString() + ":" + myCc3R.coinHoppers.hoppersDx[i].diameter[x] + ", ";
                        diametersLog += myCc3R.coinHoppers.hoppersDx[i].diameter[x] + "-";
                    }
                    if (diameters.Length > 0)
                    {
                        diameters = diameters.Substring(0, diameters.Length - 2);
                        diametersLog = diametersLog.Substring(0, diametersLog.Length - 1);
                    }
                    dgInformationTable.Rows.Add("Right", myCc3R.coinHoppers.hoppersDx[i].address.ToString("00"), myCc3R.coinHoppers.hoppersDx[i].equipCategory, myCc3R.coinHoppers.hoppersDx[i].manufacturerId,
                        myCc3R.coinHoppers.hoppersDx[i].productCode, myCc3R.coinHoppers.hoppersDx[i].serialNumber, diameters);
                    myFunc.LogWrite(logFileName, ",056RIGHT,057" + myCc3R.coinHoppers.hoppersDx[i].address.ToString("00") + ",058" + myCc3R.coinHoppers.hoppersDx[i].equipCategory + ",059"
                        + myCc3R.coinHoppers.hoppersDx[i].manufacturerId + ",05A" + myCc3R.coinHoppers.hoppersDx[i].productCode + ",05B" +
                        myCc3R.coinHoppers.hoppersDx[i].serialNumber + ",05C" + diametersLog);
                }
                grpDataGrid.Visible = true;
                dgInformationTable.AutoSize = true;
                myFunc.WriteMessage(lblMessage, 41); //Toccare lo schermo per avviare la prova dei motori degli hopper.
                myFunc.WaitingKey();

                for (int i = 0; i < myCc3R.coinHoppers.hoppersDx.Count(); i++)
                {
                    myFunc.WriteMessage(lblMessage, 31, Color.White, myCc3R.coinHoppers.hoppersDx[i].address.ToString("00")); //Verificare la corretta movimentazione dell' hopper con ID {0}
                    grpDataGrid.Visible = false;
                    HopperTestMotor(myCc3R.coinHoppers.hoppersDx[i]);
                    if (TestYesNo(testId, 27, 30, myCc3R.coinHoppers.hoppersDx[i].address.ToString("00")) != "OK") //Verifica motori hopper eseguito correttamente? 
                    {
                        testResult = "KO";
                        goto endTest;
                    }
                    grpDataGrid.Visible = true;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                testResult = "KO";
            }
        endTest:
            mySerial.CloseCom();
            grpDataGrid.Visible = false;
            if (testResult == "OK")
                myLabel[testId].ForeColor = Color.LightGreen;
            else
                myLabel[testId].ForeColor = Color.Orange;
            return testResult;
        }

        string CoinHopperSx(int testId)
        {
            protocol = "cctalk";
            testResult = "KO";
            mySerial.CloseCom();
            myLabel[testId].ForeColor = Color.LightBlue;
            try
            {
                mySerial.ConfigCom(iniConfig.GetValue("CoinHopperSx", "PortName"));
                myFunc.WriteMessage(lblMessage, 50); //Aprire la porta ed estrarre il vassoio degli hopper di sinistra. Toccare lo schermo per continuare.
                myFunc.WaitingKey();
            //ricerca hopper collegati
            startTest:
                myFunc.WriteMessage(lblTitle, 27);//HOPPER
                myFunc.WriteMessage(lblMessage, 26);//Ricerca degli Hopper collegati
                dgInformationTable.Rows.Clear();
                dgInformationTable.Columns.Clear();
                mySerial.SendCommand(myFunc.CcTalkAddressPoll());
                myCc3R.coinHoppers.hoppersSx = new STRUCTURES.SCC3RHopper[ccTalkAnswer.answer.Length / 2];
                if (mySerial.commandAnswer.Length == 0 | mySerial.commandAnswer == "NO ANSWER")
                    goto endTest;

                for (int i = 0; i < ccTalkAnswer.answer.Length / 2; i++)
                {
                    myCc3R.coinHoppers.hoppersSx[i].address = Convert.ToInt16(ccTalkAnswer.answer.Substring(i * 2, 2));
                }

                if (myCc3R.coinHoppers.hoppersSx.Length == 0)
                {
                    grpDataGrid.Visible = false;
                    RetryOrExit(303, 309, ref btnPressed); //Nessun hopper trovato. Verifica la connessione. Ripetere il test o uscire?
                    if (btnPressed == "ESC")
                    {
                        testResult = "NOHOPPER";
                        goto endTest;
                    }
                    grpDataGrid.Visible = true;
                    goto startTest;
                }
                myFunc.WaitingTime(1500);
                dgInformationTable.ColumnCount = 7;
                dgInformationTable.Columns[0].HeaderText = "Side";
                dgInformationTable.Columns[1].HeaderText = "Address";
                dgInformationTable.Columns[2].HeaderText = "Category";
                dgInformationTable.Columns[3].HeaderText = "Manuf. ID";
                dgInformationTable.Columns[4].HeaderText = "Prod. Code";
                dgInformationTable.Columns[5].HeaderText = "Serial Number";
                dgInformationTable.Columns[6].HeaderText = "Diameters";
                dgInformationTable.Columns[1].MinimumWidth = 80;
                grpDataGrid.Visible = true;
                for (int i = 0; i < myCc3R.coinHoppers.hoppersSx.Length; i++)
                {
                    myFunc.WriteMessage(lblMessage, 29); //Richiesta informazioni sugli hopper collegati
                    mySerial.SendCommand(myFunc.CcTalkGetEquipCat(myCc3R.coinHoppers.hoppersSx[i].address));
                    myCc3R.coinHoppers.hoppersSx[i].equipCategory = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkGetManufID(myCc3R.coinHoppers.hoppersSx[i].address));
                    myCc3R.coinHoppers.hoppersSx[i].manufacturerId = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkGetProdCode(myCc3R.coinHoppers.hoppersSx[i].address));
                    myCc3R.coinHoppers.hoppersSx[i].productCode = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkGetSerialNumber(myCc3R.coinHoppers.hoppersSx[i].address));
                    myCc3R.coinHoppers.hoppersSx[i].serialNumber = myFunc.Hex2Dbl(ccTalkAnswer.answer.Substring(12, 2) + ccTalkAnswer.answer.Substring(10, 2) + ccTalkAnswer.answer.Substring(8, 2)).ToString();

                    mySerial.SendCommand(myFunc.CcTalkGetDiameters(myCc3R.coinHoppers.hoppersSx[i].address));
                    myCc3R.coinHoppers.hoppersSx[i].diameter = new int[((ccTalkAnswer.answer.Length - 8) / 4)];
                    string diameters = "";
                    string diametersLog = "";
                    for (int x = 0; x < myCc3R.coinHoppers.hoppersSx[i].diameter.Length; x++)
                    {
                        myCc3R.coinHoppers.hoppersSx[i].diameter[x] = myFunc.Hex2Dec(ccTalkAnswer.answer.Substring(8 + (x * 4), 4));
                        diameters += (x + 1).ToString() + ":" + myCc3R.coinHoppers.hoppersSx[i].diameter[x] + ", ";
                        diametersLog += myCc3R.coinHoppers.hoppersSx[i].diameter[x] + "-";
                    }
                    if (diameters.Length > 0)
                    {
                        diameters = diameters.Substring(0, diameters.Length - 2);
                        diametersLog = diametersLog.Substring(0, diametersLog.Length - 1);
                    }
                    dgInformationTable.Rows.Add("Right", myCc3R.coinHoppers.hoppersSx[i].address.ToString("00"), myCc3R.coinHoppers.hoppersSx[i].equipCategory, myCc3R.coinHoppers.hoppersSx[i].manufacturerId,
                        myCc3R.coinHoppers.hoppersSx[i].productCode, myCc3R.coinHoppers.hoppersSx[i].serialNumber, diameters);
                    myFunc.LogWrite(logFileName, ",056RIGHT,057" + myCc3R.coinHoppers.hoppersSx[i].address.ToString("00") + ",058" + myCc3R.coinHoppers.hoppersSx[i].equipCategory + ",059"
                        + myCc3R.coinHoppers.hoppersSx[i].manufacturerId + ",05A" + myCc3R.coinHoppers.hoppersSx[i].productCode + ",05B" +
                        myCc3R.coinHoppers.hoppersSx[i].serialNumber + ",05C" + diametersLog);
                }
                grpDataGrid.Visible = true;
                dgInformationTable.AutoSize=true;
                myFunc.WriteMessage(lblMessage, 41); //Toccare lo schermo per avviare la prova dei motori degli hopper.
                myFunc.WaitingKey();

                for (int i = 0; i<myCc3R.coinHoppers.hoppersSx.Count();i++)
                {
                    myFunc.WriteMessage(lblMessage, 31, Color.White, myCc3R.coinHoppers.hoppersSx[i].address.ToString("00")); //Verificare la corretta movimentazione dell' hopper con ID {0}
                    grpDataGrid.Visible = false;
                    HopperTestMotor(myCc3R.coinHoppers.hoppersSx[i]);
                    if (TestYesNo(testId, 27, 30, myCc3R.coinHoppers.hoppersSx[i].address.ToString("00")) != "OK") //Verifica motori hopper eseguito correttamente? 
                    {
                        testResult = "KO";
                        goto endTest;
                    }
                    grpDataGrid.Visible = true;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                testResult = "KO";
            }
        endTest:
            mySerial.CloseCom();
            grpDataGrid.Visible = false;
            if (testResult == "OK")
                myLabel[testId].ForeColor = Color.LightGreen;
            else
                myLabel[testId].ForeColor = Color.Orange;
            return testResult;
        }

        void HopperTestMotor(STRUCTURES.SCC3RHopper hopper)// string serial, Int32 address
        {
            string hopperSn = myFunc.Dec2Hex(Convert.ToInt32(hopper.serialNumber));
            hopperSn = hopperSn.Substring(4, 2) + hopperSn.Substring(2, 2) + hopperSn.Substring(0, 2);
            mySerial.SendCommand(myFunc.CcTalkEnableHopper(hopper.address));
        startMotor:
            //toccare lo schermo per avviare il test e verificare che i motori girino correttamente
            mySerial.SendCommand(myFunc.CcTalkDispense(hopper, hopperSn));//.address, hopperSn
                                                                //I motori hanno girato correttamente?
        paymentState: 
            mySerial.SendCommand(myFunc.CcTalkPaymentState(hopper.address));
            if (ccTalkAnswer.answer.Substring(0,4)!="0101")
            {
                myFunc.WaitingTime(300);
                goto paymentState;
            }
            mySerial.SendCommand(myFunc.CcTalkEmergencyStop(hopper.address));
            
        }
        void GetIOConverterStatus()
        {
        startCommand:
            myFunc.WriteMessage(lblMessage, 23); // richiesta stato sensori
            string command = myFunc.DConCommand("@" + myCc3R.remoteIO.address);
            mySerial.OpenCom();
            mySerial.SendCommand(command);

            if (mySerial.commandAnswer=="" | mySerial.commandAnswer=="NO ANSWER")
            {
                RetryOrExit(303, 302,ref btnPressed); //ERRORE - Nessuna risposta, ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult= "NO ANSWER";
                    goto endTest;
                }
                goto startCommand;
            }
            
            if (dConAnswer.lead == ">")
            {
                for (int i = 0; i <= 7; i++)
                {
                    myCc3R.remoteIO.port[i].value = myFunc.Hex2Bin(dConAnswer.data).Substring(i, 1);
                    switch (i)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 5:
                        case 6:
                            if (myCc3R.remoteIO.port[i].value == "0")
                            { myCc3R.remoteIO.port[i].status = "OPEN"; }
                            else { myCc3R.remoteIO.port[i].status = "CLOSE"; }
                            break;
                        case 4:
                        case 7:
                            if (myCc3R.remoteIO.port[i].value == "1")
                            { myCc3R.remoteIO.port[i].status = "OPEN"; }
                            else { myCc3R.remoteIO.port[i].status = "CLOSE"; }
                            break;
                    }
                }
            }
            endTest:
            mySerial.CloseCom();
        }

        public STRUCTURES.SConnectionTest ConnectionRelayTest()
        {
            STRUCTURES.SConnectionTest result;
            result.CoinHopperDx = false;
            result.CoinHopperSx = false;
            result.CtCoin = false;
            RS232 coinHopperDxSerial = new RS232();
            RS232 coinHopperSxSerial = new RS232();
            RS232 ctcoinSerial = new RS232();

            ctcoinSerial.ConfigCom(iniConfig.GetValue("CtCoin", "PortName"));
            coinHopperDxSerial.ConfigCom(iniConfig.GetValue("CoinHopperDx", "PortName"));
            coinHopperSxSerial.ConfigCom(iniConfig.GetValue("CoinHopperSx", "PortName"));

            ctcoinSerial.OpenCom();
            ctcoinSerial.SendCommand(myFunc.CtCoinConnect());
            MessageBox.Show(ctcoinSerial.commandAnswer);
            if (ctcoinSerial.commandAnswer.Length > 0) { result.CtCoin = true; }
            ctcoinSerial.CloseCom();

            coinHopperSxSerial.OpenCom();
            coinHopperSxSerial.SendCommand(myFunc.CcTalkAddressPoll());
            MessageBox.Show(coinHopperSxSerial.commandAnswer);
            if (coinHopperSxSerial.commandAnswer.Length > 0) { result.CoinHopperSx = true; }
            coinHopperSxSerial.CloseCom();

            coinHopperDxSerial.OpenCom();
            coinHopperDxSerial.SendCommand(myFunc.CcTalkAddressPoll());
            MessageBox.Show(coinHopperDxSerial.commandAnswer);
            if (coinHopperDxSerial.commandAnswer.Length > 0) { result.CoinHopperDx = true; }
            coinHopperDxSerial.CloseCom();
            return result;
        }

        public string IOConverterTest(int testID)
        {
            mySerial.CloseCom();
            testResult = "OK";
            protocol = "dcon";
            myLabel[testID].ForeColor = Color.LightBlue;
            myCc3R.remoteIO.address= myFunc.Dec2Hex(Convert.ToInt16(iniConfig.GetValue("remotemodule", "address")),2);
            picImage.Image = CC3R.Properties.Resources.tuttoChiuso;
            picImage.Visible = true;
            mySerial.ConfigCom(iniConfig.GetValue("RemoteModule", "PortName"));

            string command = "";
            ConnectionRelayTest();
            mySerial.OpenCom();
            command = myFunc.DConCommand("@" + myCc3R.remoteIO.address + "00"); // 
            mySerial.SendCommand(command);
            command = myFunc.DConCommand("@" + myCc3R.remoteIO.address + "00");
            mySerial.SendCommand(command);
            mySerial.CloseCom();
            //apri seriale ctcoin

            //prova connessione ctcoin (deve funzionare)
            //chiudi seriale ctcoin
            //apri seriale dcon
            //spegni DO2
            command = myFunc.DConCommand("@" + myCc3R.remoteIO.address + "02");
            //ciclo

            command = myFunc.DConCommand("@" + myCc3R.remoteIO.address + "00");
            //spegni DO3
            command = myFunc.DConCommand("@" + myCc3R.remoteIO.address + "04");
            //ciclo

            command = myFunc.DConCommand("@" + myCc3R.remoteIO.address + "00");


            //RS232 myCtCoinSerial = new RS232();
            //mySerial.CloseCom();
            //mySerial.ConfigCom(iniConfig.GetValue("CTCoin", "PortName"));
            //mySerial.OpenCom();
            //mySerial.SendCommand(myFunc.CtCoinConnect());
            //MessageBox.Show(mySerial.answerByte.ToString());
            Application.Exit();






        // FINE
        // 0 = Cash till drawer RIGHT       Normal = 0 - OPEN
        // 1 = Side shelf CT Coin rail door Normal = 1 - CLOSE
        // 2 = Cash till drawer LEFT        Normal = 0 - OPEN
        // 3 = Front DOOR                   Normal = 1 - CLOSE
        // 4 = LEFT coin drawer             Normal = 0 - CLOSE
        // 5 = Side door UPPER              Normal = 1 - CLOSE
        // 6 = Side door LOWER              Normal = 1 - CLOSE
        // 7 = RIGHT coin drawer            Normal = 0 - CLOSE

        // 0 = CTCoin POWER                 Normal = 0 - CLOSE
        // 1 = Cash till drawer LEFT POWER  Normal = 0 - CLOSE
        // 2 = Cash till drawer RIGHT POWER Normal = 0 - CLOSE

        closeAll:
            myFunc.WriteMessage(lblTitle, 8);//IO CONVERTER TEST
            myFunc.WriteMessage(lblMessage, 9);//Chiudere tutte le porte e toccare lo schermo per continuare
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[0].status != "OPEN" |
                myCc3R.remoteIO.port[1].status != "CLOSE" |
                myCc3R.remoteIO.port[2].status != "OPEN" |
                myCc3R.remoteIO.port[3].status != "CLOSE" |
                myCc3R.remoteIO.port[4].status != "CLOSE" |
                myCc3R.remoteIO.port[5].status != "CLOSE" |
                myCc3R.remoteIO.port[6].status != "CLOSE" |
                myCc3R.remoteIO.port[7].status != "CLOSE")
            {
                string error ="";
                if (myCc3R.remoteIO.port[0].status != "OPEN")
                    error += myCc3R.remoteIO.port[0].description + " -> " + "CLOSED. " ;
                if (myCc3R.remoteIO.port[1].status != "CLOSE")
                    error += myCc3R.remoteIO.port[1].description + " -> " + "OPENED. " ;
                if (myCc3R.remoteIO.port[2].status != "OPEN")
                    error += myCc3R.remoteIO.port[2].description + " -> " + "CLOSED. " ;
                if (myCc3R.remoteIO.port[3].status != "CLOSE")
                    error += myCc3R.remoteIO.port[3].description + " -> " + "OPENED. " ;
                if (myCc3R.remoteIO.port[4].status != "CLOSE")
                    error += myCc3R.remoteIO.port[4].description + " -> " + "OPENED. " ;
                if (myCc3R.remoteIO.port[5].status != "CLOSE")
                    error += myCc3R.remoteIO.port[5].description + " -> " + "OPENED. " ;
                if (myCc3R.remoteIO.port[6].status != "CLOSE")
                    error += myCc3R.remoteIO.port[6].description + " -> " + "OPENED. " ;
                if (myCc3R.remoteIO.port[7].status != "CLOSE")
                    error += myCc3R.remoteIO.port[7].description + " -> " + "OPENED.";
                RetryOrExit(303, 304, ref btnPressed,error); //ERRORE - Gli sportelli e/o i micro non sono nella posizione attesa: {0}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "ALLCLOSEKO";
                    goto endTest;
                }
                goto closeAll;
            }
            
        openFrontDoor:
            myFunc.WriteMessage(lblMessage, 10); // Aprire la porta frontale e toccare lo schermo per continuare
            picImage.Image = CC3R.Properties.Resources.portaAperta;
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[3].status != "OPEN")
            {
                RetryOrExit(303, 305, ref btnPressed,myCc3R.remoteIO.port[3].description); //ERRORE - Non rilevata l'apertura del sensore {0} . Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "FDOORKO";
                    goto endTest;
                }
                goto openFrontDoor;
            }

        openRightHopper:
            //open right hopper
            myFunc.WriteMessage(lblMessage, 11); // Estrarre completamente il carrello degli hopper di DESTRA. Toccare lo schermo per continuare
            picImage.Image = CC3R.Properties.Resources.rightHopperAperto;
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[7].status != "OPEN")
            {
                RetryOrExit(303, 305, ref btnPressed, myCc3R.remoteIO.port[7].description); //ERRORE - Non rilevata l'apertura del sensore {0}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "RHOPPERKO";
                    goto endTest;
                }
                goto openRightHopper;
            }

        openLeftHopper:
            //close right hopper and open left hopper
            myFunc.WriteMessage(lblMessage, 12); // Chiudere il carrello degli hopper di destra ed estrarre il carrello di SINISTRA.. Toccare lo schermo per continuare
            picImage.Image = CC3R.Properties.Resources.leftHopperAperto;
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[4].status != "OPEN")
            {
                RetryOrExit(303, 305, ref btnPressed, myCc3R.remoteIO.port[4].description); //ERRORE - Non rilevata l'apertura del sensore {0}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "LHOPPERKO";
                    goto endTest;
                }
                goto openLeftHopper;
            }


        closeRightTillDrawer:
            //close left hopper and close righttilldrawer
            myFunc.WriteMessage(lblMessage, 13); // Chiudere il carrello degli hopper di sinistra ed azionare il micro CASH TILL DRAWER DX. Toccare lo schermo per continuare
            picImage.Image = CC3R.Properties.Resources.micro;
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[0].status != "CLOSE")
            {
                RetryOrExit(303, 306, ref btnPressed, myCc3R.remoteIO.port[0].description); //ERRORE - Non rilevata la chiusura del sensore {0}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "RTDRAWERKO";
                    goto endTest;
                }
                goto closeRightTillDrawer;
            }

        closeLeftTillDrawer:
            //close Right Till drawer and close front door
            myFunc.WriteMessage(lblMessage, 14); // Azionare il micro CASH TILL DRAWER SX. Toccare lo schermo per continuare
            picImage.Image = CC3R.Properties.Resources.micro;
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[2].status != "CLOSE")
            {
                RetryOrExit(303, 306, ref btnPressed, myCc3R.remoteIO.port[2].description); //ERRORE - Non rilevata la chiusura del sensore {0}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "LTDRAWERKO    ";
                    goto endTest;
                }
                goto closeLeftTillDrawer;
            }

        openSideDoorLower:
            //open Side Door Lower
            myFunc.WriteMessage(lblMessage, 15); // Aprire lo sportello laterale in BASSO. Toccare lo schermo per continuare
            picImage.Image = CC3R.Properties.Resources.PortaLatBassoAperta;
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[6].status != "OPEN")
            {
                RetryOrExit(303, 305, ref btnPressed, myCc3R.remoteIO.port[6].description); //ERRORE - Non rilevata l'apertura del sensore {0}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "SDLOWERKO";
                    goto endTest;
                }
                goto openSideDoorLower;
            }

        openSideDoorUpper:
            //close side door lower and open side door upper
            myFunc.WriteMessage(lblMessage, 16); // Chiudere lo sportello laterale basso ed aprire qullo in ALTO. Toccare lo schermo per continuare
            picImage.Image = CC3R.Properties.Resources.PortaLatAltoAperta;
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[5].status != "OPEN")
            {
                RetryOrExit(303, 305, ref btnPressed, myCc3R.remoteIO.port[5].description); //ERRORE - Non rilevata l'apertura del sensore {0}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "SDUPPERKO";
                    goto endTest;
                }
                goto openSideDoorUpper;
            }

        openCtCoinRail:
            //Open CT COIN rail
            myFunc.WriteMessage(lblMessage, 17); // Aprire il CT COIN RAIL. Toccare lo schermo per continuare
            picImage.Image = CC3R.Properties.Resources.CTCoinAperto;
            myFunc.WaitingKey();
            GetIOConverterStatus();
            if (myCc3R.remoteIO.port[1].status != "OPEN")
            {
                RetryOrExit(303, 305, ref btnPressed, myCc3R.remoteIO.port[1].description); //ERRORE - Non rilevata l'apertura del sensore {0}. Ripetere il test o uscire?
                if (btnPressed == "ESC")
                {
                    testResult = "CCRAILKO";
                    goto endTest;
                }
                goto openCtCoinRail;
            }

        powerTestCTCoin:
            //POWER OFF CT COIN
            


        powerTestRightTill:
            
            
        powerTestLeftTill:
            
            
        endTest:
            picImage.Visible = false;
            if (testResult == "OK")
                myLabel[testID].ForeColor = Color.LightGreen;
            else
                myLabel[testID].ForeColor = Color.Orange;

            myFunc.LogWrite(logFileName, "," + myTest.test[testID].logID + testResult);
            return testResult;
        }

        public string TestYesNo(int titleNumber, int messageNumber)
        {
            testResult = "";
            picOk.Visible = true;
            picKo.Visible = true;
            myFunc.WriteMessage(lblTitle, titleNumber);
            myFunc.WriteMessage(lblMessage, messageNumber);
            while (testResult == "")
            {
                Application.DoEvents();
            }
            picOk.Visible = false;
            picKo.Visible = false;
            lblMessage.Text = "";
            lblTitle.Text = "";
            return testResult;
        }

        /// <summary>
        /// Asking if a condition is true or false with OK/KO buttons
        /// </summary>
        /// <param name="titleNumber">Title ID of message in the language ini file</param>
        /// <param name="messageNumber">Message ID of message in the language ini file</param>
        /// <returns></returns>
        public string TestYesNo(int testIndex, int titleNumber, int messageNumber)
        {
            myLabel[testIndex].ForeColor = Color.LightBlue;
            testResult = "";
            picOk.Visible = true;
            picKo.Visible = true;
            myFunc.WriteMessage(lblTitle, titleNumber);
            myFunc.WriteMessage(lblMessage, messageNumber);
            while (testResult == "")
            {
                Application.DoEvents();
            }
            picOk.Visible = false;
            picKo.Visible = false;
            lblMessage.Text = "";
            lblTitle.Text = "";
            if (testResult=="OK")
                myLabel[testIndex].ForeColor = Color.LightGreen;
            else
                myLabel[testIndex].ForeColor = Color.Orange;

            myFunc.LogWrite(logFileName, "," + myTest.test[testIndex].logID + testResult);
            return testResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="titleNumber">Title ID of message in the language ini file</param>
        /// <param name="messageNumber">Message ID of message in the language ini file</param>
        /// <param name="messageParam">Extended parameter in the message indicated in the {}</param>
        /// <returns></returns>
        public string TestYesNo(int testIndex, int titleNumber, int messageNumber, params object[] messageParam)
        {
            testResult = "";
            picOk.Visible = true;
            picKo.Visible = true;
            myFunc.WriteMessage(lblTitle, titleNumber);
            myFunc.WriteMessage(lblMessage, messageNumber,Color.White, messageParam);
            while (testResult == "")
            {
                Application.DoEvents();
            }
            picOk.Visible = false;
            picKo.Visible = false;
            lblMessage.Text = "";
            lblTitle.Text = "";
            return testResult;
        }

        public void RetryOrExit(int titleNumber, int messageNumber,ref string btnPressed, params object[] messageParam)
        {
            //testResult = "";
            btnPressed = "";
            myFunc.keyCodePressed = 0;
            bool imageVisible = picImage.Visible;
            picRetry.Visible = true;
            picExit.Visible = true;
            if (imageVisible==true) picImage.Visible = false;
            //myFunc.WriteMessage(lblTitle,titleNumber);
            if (messageParam.Length > 0)
                myFunc.WriteMessage(lblMessage, messageNumber, myStruct.color.error,messageParam);
            else
                myFunc.WriteMessage(lblMessage,messageNumber, myStruct.color.error);
            
            //while (testResult=="")
            while (myFunc.keyCodePressed==0)
            {
                Application.DoEvents();
            }
            picRetry.Visible = false;
            picExit.Visible = false;
            lblMessage.Text = "";
            //lblTitle.Text = "";
            picImage.Visible = imageVisible;
            btnPressed= myFunc.Dec2Ascii(myFunc.keyCodePressed);
        }
        private void KeyPreddedOnForm(object sender, KeyEventArgs e)
        {
            myFunc.keyCodePressed = e.KeyValue;
            if (e.KeyValue==13)
            {
                if (txtSerialNumber.Tag.ToString() == "ProductCode")
                {
                    myCc3R.productCode = txtSerialNumber.Text;
                    txtSerialNumber.Tag = "ProductSerialNumber";
                    txtSerialNumber.Text = "CC3R";
                    myFunc.WriteMessage(lblMessage, 3); //Inserire la matricola e premere invio
                }
                else
                {
                    myCc3R.productSerialNumber = txtSerialNumber.Text;
                    txtSerialNumber.Tag = "ProductCode";
                    grpSerial.Visible = false;
                    CC3RTEST();
                }
                txtSerialNumber.SelectionStart = txtSerialNumber.Text.Length;
                txtSerialNumber.SelectionLength = 0;
            }
        }

        private void TestResultButton(object sender, EventArgs e)
        {
            PictureBox pic = (PictureBox)sender;
            switch (pic.Name.ToString())
            {
                case "picOk":
                    testResult = "OK";
                    break;
                case "picKo":
                    testResult = "KO";
                    break;
            }
            myFunc.keyCodePressed = 0;
        }

        private void ClickOnDisplay(object sender, EventArgs e)
        {
            myFunc.keyCodePressed = 13;
        }

        private void frmPrincipale_Resize(object sender, EventArgs e)
        {
            grpDcon.Location = new Point((scMain.Panel2.Width - grpDcon.Width) / 2, 150);
            grpSerial.Location = new Point((scMain.Panel2.Width - grpSerial.Width) / 2, 150);
            picOk.Location = new Point((scMain.Panel2.Width - picOk.Width) / 4, 170);
            picKo.Location = new Point(((scMain.Panel2.Width - picKo.Width) / 4) * 3, 170);
            grpDataGrid.Location=new Point((scMain.Panel2.Width - grpDataGrid.Width) / 2, 150);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            protocol = "cctalk";
            //mySerial.OpenCom();
            mySerial.SendCommand(myFunc.CcTalkCommand(0, 1, "FD")); //Address Poll
            myCc3R.coinHoppers.hoppersDx = new  STRUCTURES.SCC3RHopper[ccTalkAnswer.answer.Length / 2];
            for (int i = 0; i< ccTalkAnswer.answer.Length/2; i++)
            {
                myCc3R.coinHoppers.hoppersDx[i].address =Convert.ToInt16(ccTalkAnswer.answer.Substring(i*2, 2));
            }
            myFunc.WaitingTime(1500);
            for (int i=0;i< myCc3R.coinHoppers.hoppersDx.Count();i++)
            {
                try
                {
                    mySerial.SendCommand(myFunc.CcTalkCommand(myCc3R.coinHoppers.hoppersDx[i].address, 1, "F5")); //Request equipment category ID
                    myCc3R.coinHoppers.hoppersDx[i].equipCategory = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkCommand(myCc3R.coinHoppers.hoppersDx[i].address, 1, "F6")); //Request manufacturer ID
                    myCc3R.coinHoppers.hoppersDx[i].manufacturerId = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkCommand(myCc3R.coinHoppers.hoppersDx[i].address, 1, "F4")); //Request Product code
                    myCc3R.coinHoppers.hoppersDx[i].productCode = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkCommand(myCc3R.coinHoppers.hoppersDx[i].address, 1, "F2")); //Request Serial Number
                    myCc3R.coinHoppers.hoppersDx[i].serialNumber = myFunc.Hex2Dbl(ccTalkAnswer.answer.Substring(12, 2) + ccTalkAnswer.answer.Substring(10, 2) + ccTalkAnswer.answer.Substring(8, 2)).ToString();

                    mySerial.SendCommand(myFunc.CcTalkCommand(myCc3R.coinHoppers.hoppersDx[i].address, 1, "F6")); //Request manufacturer ID
                    myCc3R.coinHoppers.hoppersDx[i].manufacturerId = myFunc.Hex2Ascii(ccTalkAnswer.answer.Substring(8, ccTalkAnswer.answer.Length - 10));

                    mySerial.SendCommand(myFunc.CcTalkCommand(myCc3R.coinHoppers.hoppersDx[i].address, 1, "F1")); //Request Software version
                    myCc3R.coinHoppers.hoppersDx[i].firmware.version = myFunc.Hex2Dec(ccTalkAnswer.answer.Substring(8, 2)).ToString() + "." + myFunc.Hex2Dec(ccTalkAnswer.answer.Substring(10, 2)).ToString();

                    mySerial.SendCommand(myFunc.CcTalkCommand(myCc3R.coinHoppers.hoppersDx[i].address, 1, "29")); //Request Diameters
                    myCc3R.coinHoppers.hoppersDx[i].diameter = new int[((ccTalkAnswer.answer.Length-8)/4)];
                    for (int x = 0; x < myCc3R.coinHoppers.hoppersDx[i].diameter.Length - 1; x++)
                    {
                        myCc3R.coinHoppers.hoppersDx[i].diameter[i] = myFunc.Hex2Dec(ccTalkAnswer.answer.Substring(8 + (x * 4), 4));
                    }

                }
                catch (Exception ex) { }
            }
            


            mySerial.CloseCom();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mySerial.SendCommand(myFunc.CcTalkCommand(4, 1, "37")); //
            lblBottom.Text = ccTalkAnswer.answer;
            mySerial.CloseCom();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            mySerial.OpenCom();
            lblBottom.Text= mySerial.SendCommand(Constants.printerIdendity);
            mySerial.CloseCom();
        }

        private void aPSCP290PrinterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAPSPrinter frmPrinter = new frmAPSPrinter(iniConfig.GetValue("Printer","portName"));
            frmPrinter.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string command = "013639333930323734";
            mySerial.OpenCom();
            mySerial.SendCommand("0209" + command + myFunc.CtCoinCRCCalc(command) + "03");
            MessageBox.Show(mySerial.commandAnswer);
            mySerial.CloseCom();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string command = "03";
            mySerial.OpenCom();
            mySerial.SendCommand("0201" + command + myFunc.CtCoinCRCCalc(command) + "03");
            MessageBox.Show(mySerial.commandAnswer);
            mySerial.CloseCom();
        }

        private void cTCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCTCoin frmCtCoin = new frmCTCoin(iniConfig.GetValue("CTCoin", "portName"));
            frmCtCoin.ShowDialog();
        }

        private void RetryOrExitClick(object sender, EventArgs e)
        {
            PictureBox pic = (PictureBox)sender;
            switch (pic.Tag.ToString())
            {
                case "Retry":
                    //testResult = "RETRY";
                    myFunc.keyCodePressed = 82;
                    break;
                case "Exit":
                    //testResult = "EXIT";
                    myFunc.keyCodePressed = 27;
                    break;
            }
            
        }

        private void azkoiyenCoinHopperToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmHoppers frmHopper = new frmHoppers(iniConfig.GetValue("CoinHopperSx", "portName"), iniConfig.GetValue("CoinHopperDx", "portName"));
            frmHopper.ShowDialog();
        }

        private void frmPrincipale_FormClosing(object sender, FormClosingEventArgs e)
        {
            Process[] processes = Process.GetProcessesByName("cc3r");
            foreach (Process proc in processes)
            {
                proc.Kill();
            }
        }


    }
    
}
