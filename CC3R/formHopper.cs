﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ARCA;

namespace CC3R
{
    public partial class frmHoppers : Form
    {
        public RS232 mySerialDx,mySerialSx;
        public STRUCTURES myStruct;
        public FUNCTIONS myFunc;
        public string command;
        public INIFILE iniConfig;
        public STRUCTURES.SCCTalkAnswer ccTalkAnswerDx = new STRUCTURES.SCCTalkAnswer();
        public STRUCTURES.SCCTalkAnswer ccTalkAnswerSx = new STRUCTURES.SCCTalkAnswer();
        public STRUCTURES.SCC3RCoinHoppers hoppers = new STRUCTURES.SCC3RCoinHoppers();

        public frmHoppers(string comPortSx, string comPortDx)
        {
            InitializeComponent();
            InitializeComunication(comPortSx, comPortDx);

        }
        void InitializeComunication(string comPortSx, string comPortDx)
        {
            mySerialDx = new RS232();
            mySerialSx = new RS232();
            myFunc = new FUNCTIONS();
            mySerialDx.ConfigCom(comPortDx);
            mySerialSx.ConfigCom(comPortSx);
            mySerialDx.DataSent += new EventHandler(ComDataSentDx);
            mySerialDx.DataReceived += new EventHandler(ComDataReceivedDx);
            mySerialSx.DataSent += new EventHandler(ComDataSentSx);
            mySerialSx.DataReceived += new EventHandler(ComDataReceivedSx);
        }

        public bool ControlInvokeRequired(Control c, Action a)
        {
            if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
            else return false;
            return true;
        }

        public void ComDataSentDx(object sender, EventArgs e)//
        {
            if (ControlInvokeRequired(lstComScopeDx, () => ComDataSentDx(sender, null))) return;
            lstComScopeDx.Items.Add("SND: " + sender.ToString());
            ccTalkAnswerDx = new STRUCTURES.SCCTalkAnswer();
        }

        public void ComDataReceivedDx(object sender, EventArgs e)
        {
            if (ControlInvokeRequired(lstComScopeDx, () => ComDataReceivedDx(sender, null))) return;
            ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
            lstComScopeDx.Items.Add("RCV: " + (ccTalkAnswerDx.answer));
            lstComScopeDx.SelectedIndex = lstComScopeDx.Items.Count-1;
        }

        public void ComDataSentSx(object sender, EventArgs e)//
        {
            if (ControlInvokeRequired(lstComScopeSx, () => ComDataSentSx(sender, null))) return;
            lstComScopeSx.Items.Add("SND: " + sender.ToString());
            ccTalkAnswerSx = new STRUCTURES.SCCTalkAnswer();
        }

        public void ComDataReceivedSx(object sender, EventArgs e)
        {
            if (ControlInvokeRequired(lstComScopeSx, () => ComDataReceivedSx(sender, null))) return;
            ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
            lstComScopeSx.Items.Add("RCV: " + ccTalkAnswerSx.answer);
            lstComScopeSx.SelectedIndex = lstComScopeSx.Items.Count - 1;
        }

        private void frmHoppers_Load(object sender, EventArgs e)
        {
            iniConfig = new ARCA.INIFILE("setup.ini");
            mySerialDx.ConfigCom(iniConfig.GetValue("CoinHopperDx", "PortName"));
            mySerialSx.ConfigCom(iniConfig.GetValue("CoinHopperSx", "PortName"));
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            HoppersSearch();
        }

        void SingleHopperInfo(string side, int id)
        {
            if (side=="Dx")
            {
                switch (id)
                {
                    case 0:
                        lblDx03Address.Text = hoppers.hoppersDx[id].address.ToString();
                        lblDx03Manuf.Text = hoppers.hoppersDx[id].manufacturerId.ToString();
                        lblDx03Code.Text = hoppers.hoppersDx[id].productCode.ToString();
                        lblDx03Sn.Text = hoppers.hoppersDx[id].serialNumber.ToString();
                        //lblDx03Diam.Text = hoppers.hoppersDx[id].diameters.ToString();
                        break;
                    case 1:
                        lblDx04Address.Text = hoppers.hoppersDx[id].address.ToString();
                        lblDx04Manuf.Text = hoppers.hoppersDx[id].manufacturerId.ToString();
                        lblDx04Code.Text = hoppers.hoppersDx[id].productCode.ToString();
                        lblDx04Sn.Text = hoppers.hoppersDx[id].serialNumber.ToString();
                        //lblDx04Diam.Text = hoppers.hoppersDx[id].diameters.ToString();
                        break;
                    case 2:
                        lblDx05Address.Text = hoppers.hoppersDx[id].address.ToString();
                        lblDx05Manuf.Text = hoppers.hoppersDx[id].manufacturerId.ToString();
                        lblDx05Code.Text = hoppers.hoppersDx[id].productCode.ToString();
                        lblDx05Sn.Text = hoppers.hoppersDx[id].serialNumber.ToString();
                        //lblDx05Diam.Text = hoppers.hoppersDx[id].diameters.ToString();
                        break;
                    case 3:
                        lblDx06Address.Text = hoppers.hoppersDx[id].address.ToString();
                        lblDx06Manuf.Text = hoppers.hoppersDx[id].manufacturerId.ToString();
                        lblDx06Code.Text = hoppers.hoppersDx[id].productCode.ToString();
                        lblDx06Sn.Text = hoppers.hoppersDx[id].serialNumber.ToString();
                        //lblDx06Diam.Text = hoppers.hoppersDx[id].diameters.ToString();
                        break;
                }
                    
            }
            else
            {
                switch (id)
                {
                    case 0:
                        lblSx03Address.Text = hoppers.hoppersSx[id].address.ToString();
                        lblSx03Manuf.Text = hoppers.hoppersSx[id].manufacturerId.ToString();
                        lblSx03Code.Text = hoppers.hoppersSx[id].productCode.ToString();
                        lblSx03Sn.Text = hoppers.hoppersSx[id].serialNumber.ToString();
                        //lblSx03Diam.Text = hoppers.hoppersSx[id].diameters.ToString();
                        break;
                    case 1:
                        lblSx04Address.Text = hoppers.hoppersSx[id].address.ToString();
                        lblSx04Manuf.Text = hoppers.hoppersSx[id].manufacturerId.ToString();
                        lblSx04Code.Text = hoppers.hoppersSx[id].productCode.ToString();
                        lblSx04Sn.Text = hoppers.hoppersSx[id].serialNumber.ToString();
                        //lblSx04Diam.Text = hoppers.hoppersSx[id].diameters.ToString();
                        break;
                    case 2:
                        lblSx05Address.Text = hoppers.hoppersSx[id].address.ToString();
                        lblSx05Manuf.Text = hoppers.hoppersSx[id].manufacturerId.ToString();
                        lblSx05Code.Text = hoppers.hoppersSx[id].productCode.ToString();
                        lblSx05Sn.Text = hoppers.hoppersSx[id].serialNumber.ToString();
                        //lblSx05Diam.Text = hoppers.hoppersSx[id].diameters.ToString();
                        break;
                    case 3:
                        lblSx06Address.Text = hoppers.hoppersSx[id].address.ToString();
                        lblSx06Manuf.Text = hoppers.hoppersSx[id].manufacturerId.ToString();
                        lblSx06Code.Text = hoppers.hoppersSx[id].productCode.ToString();
                        lblSx06Sn.Text = hoppers.hoppersSx[id].serialNumber.ToString();
                        //lblSx06Diam.Text = hoppers.hoppersSx[id].diameters.ToString();
                        break;
                }
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            int conteggioFinale = 0;
            int conteggioIniziale = 0;
            int numberCoin = 0;
            mySerialDx.OpenCom();

            string coinQty = "";
            string serial = "";
            for (int id = 3; id<=6; id++)
            {

                mySerialDx.SendCommand(myFunc.CcTalkGetSerialNumber(id));
                ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                serial = ccTalkAnswerDx.data;
                mySerialDx.SendCommand(myFunc.CcTalkGetDiameters(id));
                ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                numberCoin = Convert.ToInt16(ccTalkAnswerDx.data.Length / 4);
                coinQty = "";
                for (int i = 1; i <= numberCoin; i++)
                    coinQty += "00FF";
                mySerialDx.SendCommand(myFunc.CcTalkCommand(id, 1, "FE"));
                mySerialDx.SendCommand(myFunc.CcTalkCommand(id, 1, "A4", "A5"));
                mySerialDx.SendCommand(myFunc.CcTalkCommand(id, 1, "20", serial + coinQty));
            }

            
            bool coin1DxEndPay = false;
            bool coin2DxEndPay = false;
            bool coin3DxEndPay = false;
            bool coin4DxEndPay = false;

            conteggioFinale = 0;
            while (coin1DxEndPay==false || coin2DxEndPay==false || coin3DxEndPay==false || coin4DxEndPay==false)
            {
                if (coin1DxEndPay==false)
                {
                    mySerialDx.SendCommand(myFunc.CcTalkGetHopperStatus(3));
                    ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                    if (ccTalkAnswerDx.data.Substring(2, 2) == "00")
                    {
                        coin1DxEndPay = true;
                        conteggioFinale += myFunc.Hex2Dec(ccTalkAnswerDx.data.Substring(4, 2));
                    }
                }

                if (coin2DxEndPay == false)
                {
                    mySerialDx.SendCommand(myFunc.CcTalkGetHopperStatus(4));
                    ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                    if (ccTalkAnswerDx.data.Substring(2, 2) == "00")
                    {
                        coin2DxEndPay = true;
                        conteggioFinale += myFunc.Hex2Dec(ccTalkAnswerDx.data.Substring(4, 2));
                    }
                }

                if (coin3DxEndPay == false)
                {
                    mySerialDx.SendCommand(myFunc.CcTalkGetHopperStatus(5));
                    ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                    if (ccTalkAnswerDx.data.Substring(2, 2) == "00")
                    {
                        coin3DxEndPay = true;
                        conteggioFinale += myFunc.Hex2Dec(ccTalkAnswerDx.data.Substring(4, 2));
                    }
                }

                if (coin4DxEndPay == false)
                {
                    mySerialDx.SendCommand(myFunc.CcTalkGetHopperStatus(6));
                    ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialDx.commandAnswer);
                    if (ccTalkAnswerDx.data.Substring(2, 2) == "00")
                    {
                        coin4DxEndPay = true;
                        conteggioFinale += myFunc.Hex2Dec(ccTalkAnswerDx.data.Substring(4, 2));
                    }
                }
            }

            //MessageBox.Show(conteggioFinale.ToString());
            mySerialDx.CloseCom();
            //MessageBox.Show((conteggioFinale - conteggioIniziale).ToString());

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int conteggioFinale = 0;
            int conteggioIniziale = 0;
            int numberCoin = 0;
            mySerialSx.OpenCom();

            string coinQty = "";
            string serial = "";
            
            for (int id = 3; id <= 6; id++)
            {

                mySerialSx.SendCommand(myFunc.CcTalkGetSerialNumber(id));
                ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                serial = ccTalkAnswerDx.data;
                mySerialSx.SendCommand(myFunc.CcTalkGetDiameters(id));
                ccTalkAnswerDx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                numberCoin = Convert.ToInt16(ccTalkAnswerDx.data.Length / 4);
                coinQty = "";
                for (int i = 1; i <= numberCoin; i++)
                    coinQty += "00FF";
                mySerialSx.SendCommand(myFunc.CcTalkCommand(id, 1, "FE"));
                mySerialSx.SendCommand(myFunc.CcTalkCommand(id, 1, "A4", "A5"));
                mySerialSx.SendCommand(myFunc.CcTalkCommand(id, 1, "20", serial + coinQty));
            }

            bool coin1SxEndPay = false;
            bool coin2SxEndPay = false;
            bool coin3SxEndPay = false;
            bool coin4SxEndPay = false;

            conteggioFinale = 0;
            while (coin1SxEndPay == false || coin2SxEndPay == false || coin3SxEndPay == false || coin4SxEndPay == false)
            {
                
                if (coin1SxEndPay == false)
                {
                    mySerialSx.SendCommand(myFunc.CcTalkGetHopperStatus(3));
                    ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                    if (ccTalkAnswerSx.data.Substring(2, 2) == "00")
                    {
                        coin1SxEndPay = true;
                        conteggioFinale += myFunc.Hex2Dec(ccTalkAnswerSx.data.Substring(4, 2));
                    }
                }

                if (coin2SxEndPay == false)
                {
                    mySerialSx.SendCommand(myFunc.CcTalkGetHopperStatus(4));
                    ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                    if (ccTalkAnswerSx.data.Substring(2, 2) == "00")
                    {
                        coin2SxEndPay = true;
                        conteggioFinale += myFunc.Hex2Dec(ccTalkAnswerSx.data.Substring(4, 2));
                    }
                }

                if (coin3SxEndPay == false)
                {
                    mySerialSx.SendCommand(myFunc.CcTalkGetHopperStatus(5));
                    ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                    if (ccTalkAnswerSx.data.Substring(2, 2) == "00")
                    {
                        coin3SxEndPay = true;
                        conteggioFinale += myFunc.Hex2Dec(ccTalkAnswerSx.data.Substring(4, 2));
                    }
                }

                if (coin4SxEndPay == false)
                {
                    mySerialSx.SendCommand(myFunc.CcTalkGetHopperStatus(6));
                    ccTalkAnswerSx = myFunc.CcTalkAnswer(mySerialSx.commandAnswer);
                    if (ccTalkAnswerSx.data.Substring(2, 2) == "00")
                    {
                        coin4SxEndPay = true;
                        conteggioFinale += myFunc.Hex2Dec(ccTalkAnswerSx.data.Substring(4, 2));
                    }
                }
            }

            //MessageBox.Show(conteggioFinale.ToString());
            mySerialSx.CloseCom();
            //MessageBox.Show((conteggioFinale - conteggioIniziale).ToString());

        }

        void HoppersSearch()
        {
            string side = "Dx";
            mySerialDx.OpenCom();
            mySerialSx.OpenCom();

            mySerialDx.SendCommand(myFunc.CcTalkAddressPoll());
            mySerialSx.SendCommand(myFunc.CcTalkAddressPoll());

            hoppers.hoppersDx = new STRUCTURES.SCC3RHopper[ccTalkAnswerDx.answer.Length / 2];
            hoppers.hoppersSx = new STRUCTURES.SCC3RHopper[ccTalkAnswerSx.answer.Length / 2];
            for (int i = 0; i < ccTalkAnswerDx.answer.Length / 2; i++)
            {
                hoppers.hoppersDx[i].address = Convert.ToInt16(ccTalkAnswerDx.answer.Substring(i * 2, 2));
            }
            for (int i = 0; i < ccTalkAnswerSx.answer.Length / 2; i++)
            {
                hoppers.hoppersSx[i].address = Convert.ToInt16(ccTalkAnswerSx.answer.Substring(i * 2, 2));
            }

            myFunc.WaitingTime(1500);

            int maxHoppersLength = hoppers.hoppersDx.Length;
            if (hoppers.hoppersSx.Length>maxHoppersLength)
                maxHoppersLength = hoppers.hoppersSx.Length;

            for (int i = 0; i < maxHoppersLength; i++)
            {
                if (i < hoppers.hoppersDx.Length)
                {
                    side = "Dx";
                    mySerialDx.SendCommand(myFunc.CcTalkGetEquipCat(hoppers.hoppersDx[i].address));
                    hoppers.hoppersDx[i].equipCategory = myFunc.Hex2Ascii(ccTalkAnswerDx.answer.Substring(8, ccTalkAnswerDx.answer.Length - 10));
                    
                    mySerialDx.SendCommand(myFunc.CcTalkGetManufID(hoppers.hoppersDx[i].address));
                    hoppers.hoppersDx[i].manufacturerId = myFunc.Hex2Ascii(ccTalkAnswerDx.answer.Substring(8, ccTalkAnswerDx.answer.Length - 10));

                    mySerialDx.SendCommand(myFunc.CcTalkGetProdCode(hoppers.hoppersDx[i].address));
                    hoppers.hoppersDx[i].productCode = myFunc.Hex2Ascii(ccTalkAnswerDx.answer.Substring(8, ccTalkAnswerDx.answer.Length - 10));

                    mySerialDx.SendCommand(myFunc.CcTalkGetSerialNumber(hoppers.hoppersDx[i].address));
                    hoppers.hoppersDx[i].serialNumber = myFunc.Hex2Dbl(ccTalkAnswerDx.answer.Substring(12, 2) + ccTalkAnswerDx.answer.Substring(10, 2) + ccTalkAnswerDx.answer.Substring(8, 2)).ToString();

                    mySerialDx.SendCommand(myFunc.CcTalkGetDiameters(hoppers.hoppersDx[i].address));
                    hoppers.hoppersDx[i].diameter = new int[((ccTalkAnswerDx.answer.Length - 8) / 4)];
                    string diametersLog = "";
                    for (int x = 0; x < hoppers.hoppersDx[i].diameter.Length; x++)
                    {
                        hoppers.hoppersDx[i].diameter[x] = myFunc.Hex2Dec(ccTalkAnswerDx.answer.Substring(8 + (x * 4), 4));
                        hoppers.hoppersDx[i].diameters += (x + 1).ToString() + ":" + hoppers.hoppersDx[i].diameter[x] + ", ";
                        diametersLog += hoppers.hoppersDx[i].diameter[x] + "-";
                    }
                    if (hoppers.hoppersDx[i].diameters.Length > 0)
                    {
                        hoppers.hoppersDx[i].diameters = hoppers.hoppersDx[i].diameters.Substring(0, hoppers.hoppersDx[i].diameters.Length - 2);
                        diametersLog = diametersLog.Substring(0, diametersLog.Length - 1);
                    }
                    SingleHopperInfo(side, i);
                }

                if (i < hoppers.hoppersSx.Length)
                {
                    side = "Sx";
                    mySerialSx.SendCommand(myFunc.CcTalkGetEquipCat(hoppers.hoppersSx[i].address));
                    hoppers.hoppersSx[i].equipCategory = myFunc.Hex2Ascii(ccTalkAnswerSx.answer.Substring(8, ccTalkAnswerSx.answer.Length - 10));

                    mySerialSx.SendCommand(myFunc.CcTalkGetManufID(hoppers.hoppersSx[i].address));
                    hoppers.hoppersSx[i].manufacturerId = myFunc.Hex2Ascii(ccTalkAnswerSx.answer.Substring(8, ccTalkAnswerSx.answer.Length - 10));

                    mySerialSx.SendCommand(myFunc.CcTalkGetProdCode(hoppers.hoppersSx[i].address));
                    hoppers.hoppersSx[i].productCode = myFunc.Hex2Ascii(ccTalkAnswerSx.answer.Substring(8, ccTalkAnswerSx.answer.Length - 10));

                    mySerialSx.SendCommand(myFunc.CcTalkGetSerialNumber(hoppers.hoppersSx[i].address));
                    hoppers.hoppersSx[i].serialNumber = myFunc.Hex2Dbl(ccTalkAnswerSx.answer.Substring(12, 2) + ccTalkAnswerSx.answer.Substring(10, 2) + ccTalkAnswerSx.answer.Substring(8, 2)).ToString();

                    mySerialSx.SendCommand(myFunc.CcTalkGetDiameters(hoppers.hoppersSx[i].address));
                    hoppers.hoppersSx[i].diameter = new int[((ccTalkAnswerSx.answer.Length - 8) / 4)];
                    hoppers.hoppersDx[i].diameters = "";
                    string diametersLog = "";
                    for (int x = 0; x < hoppers.hoppersSx[i].diameter.Length; x++)
                    {
                        hoppers.hoppersSx[i].diameter[x] = myFunc.Hex2Dec(ccTalkAnswerSx.answer.Substring(8 + (x * 4), 4));
                        hoppers.hoppersDx[i].diameters += (x + 1).ToString() + ":" + hoppers.hoppersSx[i].diameter[x] + ", ";
                        diametersLog += hoppers.hoppersSx[i].diameter[x] + "-";
                    }
                    if (hoppers.hoppersDx[i].diameters.Length > 0)
                    {
                        hoppers.hoppersDx[i].diameters = hoppers.hoppersDx[i].diameters.Substring(0, hoppers.hoppersDx[i].diameters.Length - 2);
                        diametersLog = diametersLog.Substring(0, diametersLog.Length - 1);
                    }
                    SingleHopperInfo(side, i);
                }
            }

            mySerialDx.CloseCom();
            mySerialSx.CloseCom();
        }
    }
}
