﻿namespace CC3R
{
    partial class frmPrincipale
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipale));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.moduleTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aPSCP290PrinterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eLOTouchScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sideKeyPadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.azkoiyenCoinHopperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cTCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remoteModuleIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grpDataGrid = new System.Windows.Forms.GroupBox();
            this.dgInformationTable = new System.Windows.Forms.DataGridView();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblBottom = new System.Windows.Forms.Label();
            this.grpSerial = new System.Windows.Forms.GroupBox();
            this.picEnter = new System.Windows.Forms.PictureBox();
            this.picTrash = new System.Windows.Forms.PictureBox();
            this.picN8 = new System.Windows.Forms.PictureBox();
            this.picN1 = new System.Windows.Forms.PictureBox();
            this.picN5 = new System.Windows.Forms.PictureBox();
            this.picN7 = new System.Windows.Forms.PictureBox();
            this.picN2 = new System.Windows.Forms.PictureBox();
            this.picN3 = new System.Windows.Forms.PictureBox();
            this.picN4 = new System.Windows.Forms.PictureBox();
            this.picN9 = new System.Windows.Forms.PictureBox();
            this.picN6 = new System.Windows.Forms.PictureBox();
            this.picN0 = new System.Windows.Forms.PictureBox();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.picOk = new System.Windows.Forms.PictureBox();
            this.grpDcon = new System.Windows.Forms.GroupBox();
            this.btnCheckStop = new System.Windows.Forms.Button();
            this.btnCheckIo = new System.Windows.Forms.Button();
            this.picBit7 = new System.Windows.Forms.PictureBox();
            this.picBit6 = new System.Windows.Forms.PictureBox();
            this.picBit5 = new System.Windows.Forms.PictureBox();
            this.picBit4 = new System.Windows.Forms.PictureBox();
            this.picBit3 = new System.Windows.Forms.PictureBox();
            this.picBit2 = new System.Windows.Forms.PictureBox();
            this.picBit1 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.picBit0 = new System.Windows.Forms.PictureBox();
            this.lblAnswer = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSend = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.dgRemoteModule = new System.Windows.Forms.DataGridView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAddressEnd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAddressStart = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.picKo = new System.Windows.Forms.PictureBox();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.picExit = new System.Windows.Forms.PictureBox();
            this.picRetry = new System.Windows.Forms.PictureBox();
            this.scLeft = new System.Windows.Forms.SplitContainer();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lblTestTitle = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.lstComScope = new System.Windows.Forms.ListBox();
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.menuStrip1.SuspendLayout();
            this.grpDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInformationTable)).BeginInit();
            this.grpSerial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEnter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTrash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOk)).BeginInit();
            this.grpDcon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRemoteModule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picKo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRetry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scLeft)).BeginInit();
            this.scLeft.Panel1.SuspendLayout();
            this.scLeft.Panel2.SuspendLayout();
            this.scLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel1.SuspendLayout();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moduleTestToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // moduleTestToolStripMenuItem
            // 
            this.moduleTestToolStripMenuItem.CheckOnClick = true;
            this.moduleTestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aPSCP290PrinterToolStripMenuItem,
            this.eLOTouchScreenToolStripMenuItem,
            this.sideKeyPadToolStripMenuItem,
            this.azkoiyenCoinHopperToolStripMenuItem,
            this.cTCToolStripMenuItem,
            this.remoteModuleIOToolStripMenuItem});
            this.moduleTestToolStripMenuItem.Name = "moduleTestToolStripMenuItem";
            this.moduleTestToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.moduleTestToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.moduleTestToolStripMenuItem.Text = "Module Test";
            // 
            // aPSCP290PrinterToolStripMenuItem
            // 
            this.aPSCP290PrinterToolStripMenuItem.Name = "aPSCP290PrinterToolStripMenuItem";
            this.aPSCP290PrinterToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.aPSCP290PrinterToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.aPSCP290PrinterToolStripMenuItem.Text = "APS CP290 Printer";
            this.aPSCP290PrinterToolStripMenuItem.Click += new System.EventHandler(this.aPSCP290PrinterToolStripMenuItem_Click);
            // 
            // eLOTouchScreenToolStripMenuItem
            // 
            this.eLOTouchScreenToolStripMenuItem.Name = "eLOTouchScreenToolStripMenuItem";
            this.eLOTouchScreenToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.eLOTouchScreenToolStripMenuItem.Text = "ELO Touch Screen";
            // 
            // sideKeyPadToolStripMenuItem
            // 
            this.sideKeyPadToolStripMenuItem.Name = "sideKeyPadToolStripMenuItem";
            this.sideKeyPadToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.sideKeyPadToolStripMenuItem.Text = "Side KeyPad";
            // 
            // azkoiyenCoinHopperToolStripMenuItem
            // 
            this.azkoiyenCoinHopperToolStripMenuItem.Name = "azkoiyenCoinHopperToolStripMenuItem";
            this.azkoiyenCoinHopperToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.azkoiyenCoinHopperToolStripMenuItem.Text = "Azkoiyen Coin Hopper";
            this.azkoiyenCoinHopperToolStripMenuItem.Click += new System.EventHandler(this.azkoiyenCoinHopperToolStripMenuItem_Click);
            // 
            // cTCToolStripMenuItem
            // 
            this.cTCToolStripMenuItem.Name = "cTCToolStripMenuItem";
            this.cTCToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.cTCToolStripMenuItem.Text = "CTCoin";
            this.cTCToolStripMenuItem.Click += new System.EventHandler(this.cTCToolStripMenuItem_Click);
            // 
            // remoteModuleIOToolStripMenuItem
            // 
            this.remoteModuleIOToolStripMenuItem.Name = "remoteModuleIOToolStripMenuItem";
            this.remoteModuleIOToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.remoteModuleIOToolStripMenuItem.Text = "Remote module IO";
            // 
            // grpDataGrid
            // 
            this.grpDataGrid.Controls.Add(this.dgInformationTable);
            this.grpDataGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDataGrid.ForeColor = System.Drawing.Color.White;
            this.grpDataGrid.Location = new System.Drawing.Point(32, 186);
            this.grpDataGrid.Name = "grpDataGrid";
            this.grpDataGrid.Size = new System.Drawing.Size(721, 236);
            this.grpDataGrid.TabIndex = 12;
            this.grpDataGrid.TabStop = false;
            this.grpDataGrid.Text = "Information Table";
            this.grpDataGrid.Visible = false;
            // 
            // dgInformationTable
            // 
            this.dgInformationTable.AllowUserToAddRows = false;
            this.dgInformationTable.AllowUserToDeleteRows = false;
            this.dgInformationTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgInformationTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
            this.dgInformationTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInformationTable.Location = new System.Drawing.Point(6, 23);
            this.dgInformationTable.Name = "dgInformationTable";
            this.dgInformationTable.RowHeadersVisible = false;
            this.dgInformationTable.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgInformationTable.Size = new System.Drawing.Size(707, 198);
            this.dgInformationTable.TabIndex = 0;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(1, 4);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(746, 33);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "lblTitle";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTitle.Click += new System.EventHandler(this.ClickOnDisplay);
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.Color.White;
            this.lblMessage.Location = new System.Drawing.Point(1, 48);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(746, 65);
            this.lblMessage.TabIndex = 2;
            this.lblMessage.Text = "lblMessage";
            this.lblMessage.Click += new System.EventHandler(this.ClickOnDisplay);
            // 
            // lblBottom
            // 
            this.lblBottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBottom.ForeColor = System.Drawing.Color.White;
            this.lblBottom.Location = new System.Drawing.Point(3, 679);
            this.lblBottom.Name = "lblBottom";
            this.lblBottom.Size = new System.Drawing.Size(746, 16);
            this.lblBottom.TabIndex = 3;
            this.lblBottom.Text = "lblBottom";
            // 
            // grpSerial
            // 
            this.grpSerial.Controls.Add(this.picEnter);
            this.grpSerial.Controls.Add(this.picTrash);
            this.grpSerial.Controls.Add(this.picN8);
            this.grpSerial.Controls.Add(this.picN1);
            this.grpSerial.Controls.Add(this.picN5);
            this.grpSerial.Controls.Add(this.picN7);
            this.grpSerial.Controls.Add(this.picN2);
            this.grpSerial.Controls.Add(this.picN3);
            this.grpSerial.Controls.Add(this.picN4);
            this.grpSerial.Controls.Add(this.picN9);
            this.grpSerial.Controls.Add(this.picN6);
            this.grpSerial.Controls.Add(this.picN0);
            this.grpSerial.Controls.Add(this.txtSerialNumber);
            this.grpSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSerial.ForeColor = System.Drawing.Color.White;
            this.grpSerial.Location = new System.Drawing.Point(38, 141);
            this.grpSerial.Name = "grpSerial";
            this.grpSerial.Size = new System.Drawing.Size(249, 363);
            this.grpSerial.TabIndex = 4;
            this.grpSerial.TabStop = false;
            this.grpSerial.Text = "Product Code and Serial Number";
            this.grpSerial.Visible = false;
            // 
            // picEnter
            // 
            this.picEnter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picEnter.Image = ((System.Drawing.Image)(resources.GetObject("picEnter.Image")));
            this.picEnter.Location = new System.Drawing.Point(160, 294);
            this.picEnter.Name = "picEnter";
            this.picEnter.Size = new System.Drawing.Size(60, 60);
            this.picEnter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picEnter.TabIndex = 23;
            this.picEnter.TabStop = false;
            this.picEnter.Click += new System.EventHandler(this.NumberClick);
            // 
            // picTrash
            // 
            this.picTrash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picTrash.Image = ((System.Drawing.Image)(resources.GetObject("picTrash.Image")));
            this.picTrash.Location = new System.Drawing.Point(28, 294);
            this.picTrash.Name = "picTrash";
            this.picTrash.Size = new System.Drawing.Size(60, 60);
            this.picTrash.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTrash.TabIndex = 22;
            this.picTrash.TabStop = false;
            this.picTrash.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN8
            // 
            this.picN8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN8.Image = ((System.Drawing.Image)(resources.GetObject("picN8.Image")));
            this.picN8.Location = new System.Drawing.Point(94, 96);
            this.picN8.Name = "picN8";
            this.picN8.Size = new System.Drawing.Size(60, 60);
            this.picN8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN8.TabIndex = 21;
            this.picN8.TabStop = false;
            this.picN8.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN1
            // 
            this.picN1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN1.Image = ((System.Drawing.Image)(resources.GetObject("picN1.Image")));
            this.picN1.Location = new System.Drawing.Point(28, 228);
            this.picN1.Name = "picN1";
            this.picN1.Size = new System.Drawing.Size(60, 60);
            this.picN1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN1.TabIndex = 20;
            this.picN1.TabStop = false;
            this.picN1.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN5
            // 
            this.picN5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN5.Image = ((System.Drawing.Image)(resources.GetObject("picN5.Image")));
            this.picN5.Location = new System.Drawing.Point(94, 162);
            this.picN5.Name = "picN5";
            this.picN5.Size = new System.Drawing.Size(60, 60);
            this.picN5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN5.TabIndex = 19;
            this.picN5.TabStop = false;
            this.picN5.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN7
            // 
            this.picN7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN7.Image = ((System.Drawing.Image)(resources.GetObject("picN7.Image")));
            this.picN7.Location = new System.Drawing.Point(28, 96);
            this.picN7.Name = "picN7";
            this.picN7.Size = new System.Drawing.Size(60, 60);
            this.picN7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN7.TabIndex = 18;
            this.picN7.TabStop = false;
            this.picN7.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN2
            // 
            this.picN2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN2.Image = ((System.Drawing.Image)(resources.GetObject("picN2.Image")));
            this.picN2.Location = new System.Drawing.Point(94, 228);
            this.picN2.Name = "picN2";
            this.picN2.Size = new System.Drawing.Size(60, 60);
            this.picN2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN2.TabIndex = 17;
            this.picN2.TabStop = false;
            this.picN2.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN3
            // 
            this.picN3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN3.Image = ((System.Drawing.Image)(resources.GetObject("picN3.Image")));
            this.picN3.Location = new System.Drawing.Point(160, 228);
            this.picN3.Name = "picN3";
            this.picN3.Size = new System.Drawing.Size(60, 60);
            this.picN3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN3.TabIndex = 16;
            this.picN3.TabStop = false;
            this.picN3.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN4
            // 
            this.picN4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN4.Image = ((System.Drawing.Image)(resources.GetObject("picN4.Image")));
            this.picN4.Location = new System.Drawing.Point(28, 162);
            this.picN4.Name = "picN4";
            this.picN4.Size = new System.Drawing.Size(60, 60);
            this.picN4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN4.TabIndex = 15;
            this.picN4.TabStop = false;
            this.picN4.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN9
            // 
            this.picN9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN9.Image = ((System.Drawing.Image)(resources.GetObject("picN9.Image")));
            this.picN9.Location = new System.Drawing.Point(160, 96);
            this.picN9.Name = "picN9";
            this.picN9.Size = new System.Drawing.Size(60, 60);
            this.picN9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN9.TabIndex = 14;
            this.picN9.TabStop = false;
            this.picN9.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN6
            // 
            this.picN6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN6.Image = ((System.Drawing.Image)(resources.GetObject("picN6.Image")));
            this.picN6.Location = new System.Drawing.Point(160, 162);
            this.picN6.Name = "picN6";
            this.picN6.Size = new System.Drawing.Size(60, 60);
            this.picN6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN6.TabIndex = 12;
            this.picN6.TabStop = false;
            this.picN6.Click += new System.EventHandler(this.NumberClick);
            // 
            // picN0
            // 
            this.picN0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picN0.Image = global::CC3R.Properties.Resources._0;
            this.picN0.Location = new System.Drawing.Point(94, 294);
            this.picN0.Name = "picN0";
            this.picN0.Size = new System.Drawing.Size(60, 60);
            this.picN0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picN0.TabIndex = 11;
            this.picN0.TabStop = false;
            this.picN0.Click += new System.EventHandler(this.NumberClick);
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerialNumber.Location = new System.Drawing.Point(28, 42);
            this.txtSerialNumber.MaxLength = 12;
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(192, 29);
            this.txtSerialNumber.TabIndex = 9;
            this.txtSerialNumber.Tag = "ProductCode";
            this.txtSerialNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyPreddedOnForm);
            // 
            // picOk
            // 
            this.picOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picOk.Image = ((System.Drawing.Image)(resources.GetObject("picOk.Image")));
            this.picOk.Location = new System.Drawing.Point(459, 64);
            this.picOk.Name = "picOk";
            this.picOk.Size = new System.Drawing.Size(140, 140);
            this.picOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOk.TabIndex = 5;
            this.picOk.TabStop = false;
            this.picOk.Visible = false;
            this.picOk.Click += new System.EventHandler(this.TestResultButton);
            // 
            // grpDcon
            // 
            this.grpDcon.Controls.Add(this.btnCheckStop);
            this.grpDcon.Controls.Add(this.btnCheckIo);
            this.grpDcon.Controls.Add(this.picBit7);
            this.grpDcon.Controls.Add(this.picBit6);
            this.grpDcon.Controls.Add(this.picBit5);
            this.grpDcon.Controls.Add(this.picBit4);
            this.grpDcon.Controls.Add(this.picBit3);
            this.grpDcon.Controls.Add(this.picBit2);
            this.grpDcon.Controls.Add(this.picBit1);
            this.grpDcon.Controls.Add(this.label13);
            this.grpDcon.Controls.Add(this.label14);
            this.grpDcon.Controls.Add(this.label15);
            this.grpDcon.Controls.Add(this.label16);
            this.grpDcon.Controls.Add(this.label17);
            this.grpDcon.Controls.Add(this.label18);
            this.grpDcon.Controls.Add(this.label19);
            this.grpDcon.Controls.Add(this.label20);
            this.grpDcon.Controls.Add(this.label12);
            this.grpDcon.Controls.Add(this.label11);
            this.grpDcon.Controls.Add(this.label10);
            this.grpDcon.Controls.Add(this.label9);
            this.grpDcon.Controls.Add(this.label8);
            this.grpDcon.Controls.Add(this.label7);
            this.grpDcon.Controls.Add(this.label6);
            this.grpDcon.Controls.Add(this.label5);
            this.grpDcon.Controls.Add(this.picBit0);
            this.grpDcon.Controls.Add(this.lblAnswer);
            this.grpDcon.Controls.Add(this.label4);
            this.grpDcon.Controls.Add(this.txtSend);
            this.grpDcon.Controls.Add(this.btnSend);
            this.grpDcon.Controls.Add(this.dgRemoteModule);
            this.grpDcon.Controls.Add(this.btnSearch);
            this.grpDcon.Controls.Add(this.label3);
            this.grpDcon.Controls.Add(this.txtAddressEnd);
            this.grpDcon.Controls.Add(this.label2);
            this.grpDcon.Controls.Add(this.txtAddressStart);
            this.grpDcon.Controls.Add(this.label1);
            this.grpDcon.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDcon.ForeColor = System.Drawing.Color.White;
            this.grpDcon.Location = new System.Drawing.Point(576, 214);
            this.grpDcon.Name = "grpDcon";
            this.grpDcon.Size = new System.Drawing.Size(382, 496);
            this.grpDcon.TabIndex = 0;
            this.grpDcon.TabStop = false;
            this.grpDcon.Text = "DCON protocol";
            this.grpDcon.Visible = false;
            // 
            // btnCheckStop
            // 
            this.btnCheckStop.ForeColor = System.Drawing.Color.Black;
            this.btnCheckStop.Location = new System.Drawing.Point(127, 237);
            this.btnCheckStop.Name = "btnCheckStop";
            this.btnCheckStop.Size = new System.Drawing.Size(75, 23);
            this.btnCheckStop.TabIndex = 37;
            this.btnCheckStop.Text = "Stop check";
            this.btnCheckStop.UseVisualStyleBackColor = true;
            this.btnCheckStop.Click += new System.EventHandler(this.btnCheckStop_Click);
            // 
            // btnCheckIo
            // 
            this.btnCheckIo.ForeColor = System.Drawing.Color.Black;
            this.btnCheckIo.Location = new System.Drawing.Point(34, 237);
            this.btnCheckIo.Name = "btnCheckIo";
            this.btnCheckIo.Size = new System.Drawing.Size(75, 23);
            this.btnCheckIo.TabIndex = 36;
            this.btnCheckIo.Text = "Check IO";
            this.btnCheckIo.UseVisualStyleBackColor = true;
            this.btnCheckIo.Click += new System.EventHandler(this.btnCheckIo_Click);
            // 
            // picBit7
            // 
            this.picBit7.Image = ((System.Drawing.Image)(resources.GetObject("picBit7.Image")));
            this.picBit7.Location = new System.Drawing.Point(84, 455);
            this.picBit7.Name = "picBit7";
            this.picBit7.Size = new System.Drawing.Size(25, 25);
            this.picBit7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBit7.TabIndex = 35;
            this.picBit7.TabStop = false;
            // 
            // picBit6
            // 
            this.picBit6.Image = ((System.Drawing.Image)(resources.GetObject("picBit6.Image")));
            this.picBit6.Location = new System.Drawing.Point(84, 428);
            this.picBit6.Name = "picBit6";
            this.picBit6.Size = new System.Drawing.Size(25, 25);
            this.picBit6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBit6.TabIndex = 34;
            this.picBit6.TabStop = false;
            // 
            // picBit5
            // 
            this.picBit5.Image = ((System.Drawing.Image)(resources.GetObject("picBit5.Image")));
            this.picBit5.Location = new System.Drawing.Point(84, 401);
            this.picBit5.Name = "picBit5";
            this.picBit5.Size = new System.Drawing.Size(25, 25);
            this.picBit5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBit5.TabIndex = 33;
            this.picBit5.TabStop = false;
            // 
            // picBit4
            // 
            this.picBit4.Image = ((System.Drawing.Image)(resources.GetObject("picBit4.Image")));
            this.picBit4.Location = new System.Drawing.Point(84, 374);
            this.picBit4.Name = "picBit4";
            this.picBit4.Size = new System.Drawing.Size(25, 25);
            this.picBit4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBit4.TabIndex = 32;
            this.picBit4.TabStop = false;
            // 
            // picBit3
            // 
            this.picBit3.Image = ((System.Drawing.Image)(resources.GetObject("picBit3.Image")));
            this.picBit3.Location = new System.Drawing.Point(84, 347);
            this.picBit3.Name = "picBit3";
            this.picBit3.Size = new System.Drawing.Size(25, 25);
            this.picBit3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBit3.TabIndex = 31;
            this.picBit3.TabStop = false;
            // 
            // picBit2
            // 
            this.picBit2.Image = ((System.Drawing.Image)(resources.GetObject("picBit2.Image")));
            this.picBit2.Location = new System.Drawing.Point(84, 320);
            this.picBit2.Name = "picBit2";
            this.picBit2.Size = new System.Drawing.Size(25, 25);
            this.picBit2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBit2.TabIndex = 30;
            this.picBit2.TabStop = false;
            // 
            // picBit1
            // 
            this.picBit1.Image = ((System.Drawing.Image)(resources.GetObject("picBit1.Image")));
            this.picBit1.Location = new System.Drawing.Point(84, 293);
            this.picBit1.Name = "picBit1";
            this.picBit1.Size = new System.Drawing.Size(25, 25);
            this.picBit1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBit1.TabIndex = 29;
            this.picBit1.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(124, 461);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 18);
            this.label13.TabIndex = 28;
            this.label13.Text = "Cash till drawer right";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(124, 434);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(189, 18);
            this.label14.TabIndex = 27;
            this.label14.Text = "Side shelf CT Coin rail door";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(124, 407);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(132, 18);
            this.label15.TabIndex = 26;
            this.label15.Text = "Cash till drawer left";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(124, 380);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 18);
            this.label16.TabIndex = 25;
            this.label16.Text = "Front door";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(124, 353);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 18);
            this.label17.TabIndex = 24;
            this.label17.Text = "Left coin drawer";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(124, 326);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 18);
            this.label18.TabIndex = 23;
            this.label18.Text = "Side door upper";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(124, 299);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 18);
            this.label19.TabIndex = 22;
            this.label19.Text = "Side door lower";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(124, 272);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(123, 18);
            this.label20.TabIndex = 21;
            this.label20.Text = "Right coin drawer";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(42, 461);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 18);
            this.label12.TabIndex = 20;
            this.label12.Text = "Bit 7";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(42, 434);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "Bit 6";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(42, 407);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 18);
            this.label10.TabIndex = 18;
            this.label10.Text = "Bit 5";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(42, 380);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 18);
            this.label9.TabIndex = 17;
            this.label9.Text = "Bit 4";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(42, 353);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 18);
            this.label8.TabIndex = 16;
            this.label8.Text = "Bit 3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(42, 326);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 18);
            this.label7.TabIndex = 15;
            this.label7.Text = "Bit 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 299);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "Bit 1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 272);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 18);
            this.label5.TabIndex = 13;
            this.label5.Text = "Bit 0";
            // 
            // picBit0
            // 
            this.picBit0.Image = ((System.Drawing.Image)(resources.GetObject("picBit0.Image")));
            this.picBit0.Location = new System.Drawing.Point(84, 266);
            this.picBit0.Name = "picBit0";
            this.picBit0.Size = new System.Drawing.Size(25, 25);
            this.picBit0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBit0.TabIndex = 11;
            this.picBit0.TabStop = false;
            // 
            // lblAnswer
            // 
            this.lblAnswer.AutoSize = true;
            this.lblAnswer.Location = new System.Drawing.Point(90, 203);
            this.lblAnswer.Name = "lblAnswer";
            this.lblAnswer.Size = new System.Drawing.Size(24, 18);
            this.lblAnswer.TabIndex = 10;
            this.lblAnswer.Text = "??";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 18);
            this.label4.TabIndex = 9;
            this.label4.Text = "Answer";
            // 
            // txtSend
            // 
            this.txtSend.Location = new System.Drawing.Point(91, 165);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(100, 24);
            this.txtSend.TabIndex = 8;
            // 
            // btnSend
            // 
            this.btnSend.ForeColor = System.Drawing.Color.Black;
            this.btnSend.Location = new System.Drawing.Point(9, 165);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 7;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // dgRemoteModule
            // 
            this.dgRemoteModule.AllowUserToAddRows = false;
            this.dgRemoteModule.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgRemoteModule.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgRemoteModule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRemoteModule.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgRemoteModule.Location = new System.Drawing.Point(9, 92);
            this.dgRemoteModule.MultiSelect = false;
            this.dgRemoteModule.Name = "dgRemoteModule";
            this.dgRemoteModule.RowHeadersVisible = false;
            this.dgRemoteModule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRemoteModule.Size = new System.Drawing.Size(308, 46);
            this.dgRemoteModule.TabIndex = 6;
            // 
            // btnSearch
            // 
            this.btnSearch.ForeColor = System.Drawing.Color.Black;
            this.btnSearch.Location = new System.Drawing.Point(220, 46);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(97, 23);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "Search Module";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(161, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "END";
            // 
            // txtAddressEnd
            // 
            this.txtAddressEnd.Location = new System.Drawing.Point(161, 48);
            this.txtAddressEnd.Name = "txtAddressEnd";
            this.txtAddressEnd.Size = new System.Drawing.Size(53, 24);
            this.txtAddressEnd.TabIndex = 3;
            this.txtAddressEnd.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(90, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "START";
            // 
            // txtAddressStart
            // 
            this.txtAddressStart.Location = new System.Drawing.Point(90, 48);
            this.txtAddressStart.Name = "txtAddressStart";
            this.txtAddressStart.Size = new System.Drawing.Size(53, 24);
            this.txtAddressStart.TabIndex = 1;
            this.txtAddressStart.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Address range:";
            // 
            // picKo
            // 
            this.picKo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picKo.Image = ((System.Drawing.Image)(resources.GetObject("picKo.Image")));
            this.picKo.Location = new System.Drawing.Point(313, 63);
            this.picKo.Name = "picKo";
            this.picKo.Size = new System.Drawing.Size(140, 140);
            this.picKo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picKo.TabIndex = 6;
            this.picKo.TabStop = false;
            this.picKo.Visible = false;
            this.picKo.Click += new System.EventHandler(this.TestResultButton);
            // 
            // picImage
            // 
            this.picImage.Image = global::CC3R.Properties.Resources.CTCoinAperto;
            this.picImage.Location = new System.Drawing.Point(363, 358);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(478, 418);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picImage.TabIndex = 9;
            this.picImage.TabStop = false;
            this.picImage.Visible = false;
            this.picImage.Click += new System.EventHandler(this.ClickOnDisplay);
            // 
            // picExit
            // 
            this.picExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picExit.Image = global::CC3R.Properties.Resources.Exit;
            this.picExit.Location = new System.Drawing.Point(550, 40);
            this.picExit.Name = "picExit";
            this.picExit.Size = new System.Drawing.Size(140, 140);
            this.picExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picExit.TabIndex = 10;
            this.picExit.TabStop = false;
            this.picExit.Tag = "Exit";
            this.picExit.Visible = false;
            this.picExit.Click += new System.EventHandler(this.RetryOrExitClick);
            // 
            // picRetry
            // 
            this.picRetry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.picRetry.Image = global::CC3R.Properties.Resources.retry;
            this.picRetry.Location = new System.Drawing.Point(587, 64);
            this.picRetry.Name = "picRetry";
            this.picRetry.Size = new System.Drawing.Size(140, 140);
            this.picRetry.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRetry.TabIndex = 11;
            this.picRetry.TabStop = false;
            this.picRetry.Tag = "Retry";
            this.picRetry.Visible = false;
            this.picRetry.Click += new System.EventHandler(this.RetryOrExitClick);
            // 
            // scLeft
            // 
            this.scLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scLeft.Location = new System.Drawing.Point(0, 0);
            this.scLeft.Name = "scLeft";
            this.scLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scLeft.Panel1
            // 
            this.scLeft.Panel1.Controls.Add(this.picLogo);
            this.scLeft.Panel1MinSize = 80;
            // 
            // scLeft.Panel2
            // 
            this.scLeft.Panel2.Controls.Add(this.lblTestTitle);
            this.scLeft.Panel2.Controls.Add(this.button5);
            this.scLeft.Panel2.Controls.Add(this.button4);
            this.scLeft.Panel2.Controls.Add(this.button3);
            this.scLeft.Panel2.Controls.Add(this.button2);
            this.scLeft.Panel2.Controls.Add(this.button1);
            this.scLeft.Panel2.Controls.Add(this.label21);
            this.scLeft.Panel2.Controls.Add(this.lstComScope);
            this.scLeft.Panel2.Click += new System.EventHandler(this.ClickOnDisplay);
            this.scLeft.Size = new System.Drawing.Size(250, 705);
            this.scLeft.SplitterDistance = 80;
            this.scLeft.TabIndex = 0;
            this.scLeft.TabStop = false;
            // 
            // picLogo
            // 
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(0, 4);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(250, 70);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            this.picLogo.Click += new System.EventHandler(this.ClickOnDisplay);
            // 
            // lblTestTitle
            // 
            this.lblTestTitle.AutoSize = true;
            this.lblTestTitle.ForeColor = System.Drawing.Color.White;
            this.lblTestTitle.Location = new System.Drawing.Point(2, 0);
            this.lblTestTitle.Name = "lblTestTitle";
            this.lblTestTitle.Size = new System.Drawing.Size(35, 13);
            this.lblTestTitle.TabIndex = 7;
            this.lblTestTitle.Text = "TEST";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(107, 202);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(85, 50);
            this.button5.TabIndex = 6;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(11, 202);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(84, 56);
            this.button4.TabIndex = 5;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(70, 258);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(146, 55);
            this.button3.TabIndex = 4;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(67, 405);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(149, 63);
            this.button2.TabIndex = 3;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(55, 341);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 70);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(3, 479);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "ComScope";
            // 
            // lstComScope
            // 
            this.lstComScope.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstComScope.FormattingEnabled = true;
            this.lstComScope.Location = new System.Drawing.Point(3, 495);
            this.lstComScope.Name = "lstComScope";
            this.lstComScope.Size = new System.Drawing.Size(242, 121);
            this.lstComScope.TabIndex = 0;
            // 
            // scMain
            // 
            this.scMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMain.Location = new System.Drawing.Point(0, 24);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            this.scMain.Panel1.Controls.Add(this.scLeft);
            this.scMain.Panel1MinSize = 250;
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.Controls.Add(this.picRetry);
            this.scMain.Panel2.Controls.Add(this.picExit);
            this.scMain.Panel2.Controls.Add(this.picImage);
            this.scMain.Panel2.Controls.Add(this.picKo);
            this.scMain.Panel2.Controls.Add(this.grpDcon);
            this.scMain.Panel2.Controls.Add(this.picOk);
            this.scMain.Panel2.Controls.Add(this.grpSerial);
            this.scMain.Panel2.Controls.Add(this.lblBottom);
            this.scMain.Panel2.Controls.Add(this.lblMessage);
            this.scMain.Panel2.Controls.Add(this.lblTitle);
            this.scMain.Panel2.Controls.Add(this.grpDataGrid);
            this.scMain.Panel2.Click += new System.EventHandler(this.ClickOnDisplay);
            this.scMain.Size = new System.Drawing.Size(1008, 705);
            this.scMain.SplitterDistance = 250;
            this.scMain.TabIndex = 1;
            this.scMain.TabStop = false;
            // 
            // frmPrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.scMain);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1024, 768);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "frmPrincipale";
            this.Text = "CC3R TEST SW ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPrincipale_FormClosing);
            this.Load += new System.EventHandler(this.frmPrincipale_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyPreddedOnForm);
            this.Resize += new System.EventHandler(this.frmPrincipale_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInformationTable)).EndInit();
            this.grpSerial.ResumeLayout(false);
            this.grpSerial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEnter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTrash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picN0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOk)).EndInit();
            this.grpDcon.ResumeLayout(false);
            this.grpDcon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBit0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgRemoteModule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picKo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRetry)).EndInit();
            this.scLeft.Panel1.ResumeLayout(false);
            this.scLeft.Panel2.ResumeLayout(false);
            this.scLeft.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scLeft)).EndInit();
            this.scLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.scMain.Panel1.ResumeLayout(false);
            this.scMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem moduleTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eLOTouchScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sideKeyPadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem azkoiyenCoinHopperToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aPSCP290PrinterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cTCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remoteModuleIOToolStripMenuItem;
        private System.Windows.Forms.GroupBox grpDataGrid;
        private System.Windows.Forms.DataGridView dgInformationTable;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblBottom;
        private System.Windows.Forms.GroupBox grpSerial;
        private System.Windows.Forms.PictureBox picEnter;
        private System.Windows.Forms.PictureBox picTrash;
        private System.Windows.Forms.PictureBox picN8;
        private System.Windows.Forms.PictureBox picN1;
        private System.Windows.Forms.PictureBox picN5;
        private System.Windows.Forms.PictureBox picN7;
        private System.Windows.Forms.PictureBox picN2;
        private System.Windows.Forms.PictureBox picN3;
        private System.Windows.Forms.PictureBox picN4;
        private System.Windows.Forms.PictureBox picN9;
        private System.Windows.Forms.PictureBox picN6;
        private System.Windows.Forms.PictureBox picN0;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.PictureBox picOk;
        private System.Windows.Forms.GroupBox grpDcon;
        private System.Windows.Forms.Button btnCheckStop;
        private System.Windows.Forms.Button btnCheckIo;
        private System.Windows.Forms.PictureBox picBit7;
        private System.Windows.Forms.PictureBox picBit6;
        private System.Windows.Forms.PictureBox picBit5;
        private System.Windows.Forms.PictureBox picBit4;
        private System.Windows.Forms.PictureBox picBit3;
        private System.Windows.Forms.PictureBox picBit2;
        private System.Windows.Forms.PictureBox picBit1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox picBit0;
        private System.Windows.Forms.Label lblAnswer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSend;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.DataGridView dgRemoteModule;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAddressEnd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAddressStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picKo;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.PictureBox picExit;
        private System.Windows.Forms.PictureBox picRetry;
        private System.Windows.Forms.SplitContainer scLeft;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label lblTestTitle;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ListBox lstComScope;
        private System.Windows.Forms.SplitContainer scMain;
    }
}

