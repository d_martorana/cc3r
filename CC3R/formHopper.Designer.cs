﻿namespace CC3R
{
    partial class frmHoppers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpDx06 = new System.Windows.Forms.GroupBox();
            this.lblDx06Manuf = new System.Windows.Forms.Label();
            this.lblDx06Code = new System.Windows.Forms.Label();
            this.lblDx06Sn = new System.Windows.Forms.Label();
            this.lblDx06Diam = new System.Windows.Forms.Label();
            this.lblDx06Address = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.grpDx05 = new System.Windows.Forms.GroupBox();
            this.lblDx05Manuf = new System.Windows.Forms.Label();
            this.lblDx05Code = new System.Windows.Forms.Label();
            this.lblDx05Sn = new System.Windows.Forms.Label();
            this.lblDx05Diam = new System.Windows.Forms.Label();
            this.lblDx05Address = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.grpDx04 = new System.Windows.Forms.GroupBox();
            this.lblDx04Manuf = new System.Windows.Forms.Label();
            this.lblDx04Code = new System.Windows.Forms.Label();
            this.lblDx04Sn = new System.Windows.Forms.Label();
            this.lblDx04Diam = new System.Windows.Forms.Label();
            this.lblDx04Address = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.grpDx03 = new System.Windows.Forms.GroupBox();
            this.lblDx03Manuf = new System.Windows.Forms.Label();
            this.lblDx03Code = new System.Windows.Forms.Label();
            this.lblDx03Sn = new System.Windows.Forms.Label();
            this.lblDx03Diam = new System.Windows.Forms.Label();
            this.lblDx03Address = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.labelxx = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.grpSx06 = new System.Windows.Forms.GroupBox();
            this.lblSx06Manuf = new System.Windows.Forms.Label();
            this.lblSx06Code = new System.Windows.Forms.Label();
            this.lblSx06Sn = new System.Windows.Forms.Label();
            this.lblSx06Diam = new System.Windows.Forms.Label();
            this.lblSx06Address = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.grpSx05 = new System.Windows.Forms.GroupBox();
            this.lblSx05Manuf = new System.Windows.Forms.Label();
            this.lblSx05Code = new System.Windows.Forms.Label();
            this.lblSx05Sn = new System.Windows.Forms.Label();
            this.lblSx05Diam = new System.Windows.Forms.Label();
            this.lblSx05Address = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.grpSx04 = new System.Windows.Forms.GroupBox();
            this.lblSx04Manuf = new System.Windows.Forms.Label();
            this.lblSx04Code = new System.Windows.Forms.Label();
            this.lblSx04Sn = new System.Windows.Forms.Label();
            this.lblSx04Diam = new System.Windows.Forms.Label();
            this.lblSx04Address = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.grpSx03 = new System.Windows.Forms.GroupBox();
            this.lblSx03Manuf = new System.Windows.Forms.Label();
            this.lblSx03Code = new System.Windows.Forms.Label();
            this.lblSx03Sn = new System.Windows.Forms.Label();
            this.lblSx03Diam = new System.Windows.Forms.Label();
            this.lblSx03Address = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lstComScopeSx = new System.Windows.Forms.ListBox();
            this.lstComScopeDx = new System.Windows.Forms.ListBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.grpDx06.SuspendLayout();
            this.grpDx05.SuspendLayout();
            this.grpDx04.SuspendLayout();
            this.grpDx03.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grpSx06.SuspendLayout();
            this.grpSx05.SuspendLayout();
            this.grpSx04.SuspendLayout();
            this.grpSx03.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grpDx06);
            this.groupBox1.Controls.Add(this.grpDx05);
            this.groupBox1.Controls.Add(this.grpDx04);
            this.groupBox1.Controls.Add(this.grpDx03);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(415, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 413);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hopper DX";
            // 
            // grpDx06
            // 
            this.grpDx06.Controls.Add(this.lblDx06Manuf);
            this.grpDx06.Controls.Add(this.lblDx06Code);
            this.grpDx06.Controls.Add(this.lblDx06Sn);
            this.grpDx06.Controls.Add(this.lblDx06Diam);
            this.grpDx06.Controls.Add(this.lblDx06Address);
            this.grpDx06.Controls.Add(this.label56);
            this.grpDx06.Controls.Add(this.label57);
            this.grpDx06.Controls.Add(this.label58);
            this.grpDx06.Controls.Add(this.label59);
            this.grpDx06.Controls.Add(this.label60);
            this.grpDx06.ForeColor = System.Drawing.Color.White;
            this.grpDx06.Location = new System.Drawing.Point(6, 314);
            this.grpDx06.Name = "grpDx06";
            this.grpDx06.Size = new System.Drawing.Size(166, 92);
            this.grpDx06.TabIndex = 14;
            this.grpDx06.TabStop = false;
            this.grpDx06.Text = "Hopper ID:";
            // 
            // lblDx06Manuf
            // 
            this.lblDx06Manuf.AutoSize = true;
            this.lblDx06Manuf.Location = new System.Drawing.Point(84, 29);
            this.lblDx06Manuf.Name = "lblDx06Manuf";
            this.lblDx06Manuf.Size = new System.Drawing.Size(13, 13);
            this.lblDx06Manuf.TabIndex = 9;
            this.lblDx06Manuf.Text = "--";
            // 
            // lblDx06Code
            // 
            this.lblDx06Code.AutoSize = true;
            this.lblDx06Code.Location = new System.Drawing.Point(84, 42);
            this.lblDx06Code.Name = "lblDx06Code";
            this.lblDx06Code.Size = new System.Drawing.Size(13, 13);
            this.lblDx06Code.TabIndex = 8;
            this.lblDx06Code.Text = "--";
            // 
            // lblDx06Sn
            // 
            this.lblDx06Sn.AutoSize = true;
            this.lblDx06Sn.Location = new System.Drawing.Point(84, 55);
            this.lblDx06Sn.Name = "lblDx06Sn";
            this.lblDx06Sn.Size = new System.Drawing.Size(13, 13);
            this.lblDx06Sn.TabIndex = 7;
            this.lblDx06Sn.Text = "--";
            // 
            // lblDx06Diam
            // 
            this.lblDx06Diam.AutoSize = true;
            this.lblDx06Diam.Location = new System.Drawing.Point(84, 68);
            this.lblDx06Diam.Name = "lblDx06Diam";
            this.lblDx06Diam.Size = new System.Drawing.Size(13, 13);
            this.lblDx06Diam.TabIndex = 6;
            this.lblDx06Diam.Text = "--";
            // 
            // lblDx06Address
            // 
            this.lblDx06Address.AutoSize = true;
            this.lblDx06Address.Location = new System.Drawing.Point(84, 16);
            this.lblDx06Address.Name = "lblDx06Address";
            this.lblDx06Address.Size = new System.Drawing.Size(13, 13);
            this.lblDx06Address.TabIndex = 5;
            this.lblDx06Address.Text = "--";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 29);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(73, 13);
            this.label56.TabIndex = 4;
            this.label56.Text = "Manufacturer:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(6, 42);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(75, 13);
            this.label57.TabIndex = 3;
            this.label57.Text = "Product Code:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 55);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(76, 13);
            this.label58.TabIndex = 2;
            this.label58.Text = "Serial Number:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 68);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(57, 13);
            this.label59.TabIndex = 1;
            this.label59.Text = "Diameters:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 16);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(48, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "Address:";
            // 
            // grpDx05
            // 
            this.grpDx05.Controls.Add(this.lblDx05Manuf);
            this.grpDx05.Controls.Add(this.lblDx05Code);
            this.grpDx05.Controls.Add(this.lblDx05Sn);
            this.grpDx05.Controls.Add(this.lblDx05Diam);
            this.grpDx05.Controls.Add(this.lblDx05Address);
            this.grpDx05.Controls.Add(this.label66);
            this.grpDx05.Controls.Add(this.label67);
            this.grpDx05.Controls.Add(this.label68);
            this.grpDx05.Controls.Add(this.label69);
            this.grpDx05.Controls.Add(this.label70);
            this.grpDx05.ForeColor = System.Drawing.Color.White;
            this.grpDx05.Location = new System.Drawing.Point(6, 216);
            this.grpDx05.Name = "grpDx05";
            this.grpDx05.Size = new System.Drawing.Size(166, 92);
            this.grpDx05.TabIndex = 13;
            this.grpDx05.TabStop = false;
            this.grpDx05.Text = "Hopper ID:";
            // 
            // lblDx05Manuf
            // 
            this.lblDx05Manuf.AutoSize = true;
            this.lblDx05Manuf.Location = new System.Drawing.Point(84, 29);
            this.lblDx05Manuf.Name = "lblDx05Manuf";
            this.lblDx05Manuf.Size = new System.Drawing.Size(13, 13);
            this.lblDx05Manuf.TabIndex = 9;
            this.lblDx05Manuf.Text = "--";
            // 
            // lblDx05Code
            // 
            this.lblDx05Code.AutoSize = true;
            this.lblDx05Code.Location = new System.Drawing.Point(84, 42);
            this.lblDx05Code.Name = "lblDx05Code";
            this.lblDx05Code.Size = new System.Drawing.Size(13, 13);
            this.lblDx05Code.TabIndex = 8;
            this.lblDx05Code.Text = "--";
            // 
            // lblDx05Sn
            // 
            this.lblDx05Sn.AutoSize = true;
            this.lblDx05Sn.Location = new System.Drawing.Point(84, 55);
            this.lblDx05Sn.Name = "lblDx05Sn";
            this.lblDx05Sn.Size = new System.Drawing.Size(13, 13);
            this.lblDx05Sn.TabIndex = 7;
            this.lblDx05Sn.Text = "--";
            // 
            // lblDx05Diam
            // 
            this.lblDx05Diam.AutoSize = true;
            this.lblDx05Diam.Location = new System.Drawing.Point(84, 68);
            this.lblDx05Diam.Name = "lblDx05Diam";
            this.lblDx05Diam.Size = new System.Drawing.Size(13, 13);
            this.lblDx05Diam.TabIndex = 6;
            this.lblDx05Diam.Text = "--";
            // 
            // lblDx05Address
            // 
            this.lblDx05Address.AutoSize = true;
            this.lblDx05Address.Location = new System.Drawing.Point(84, 16);
            this.lblDx05Address.Name = "lblDx05Address";
            this.lblDx05Address.Size = new System.Drawing.Size(13, 13);
            this.lblDx05Address.TabIndex = 5;
            this.lblDx05Address.Text = "--";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 29);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(73, 13);
            this.label66.TabIndex = 4;
            this.label66.Text = "Manufacturer:";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(6, 42);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(75, 13);
            this.label67.TabIndex = 3;
            this.label67.Text = "Product Code:";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 55);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(76, 13);
            this.label68.TabIndex = 2;
            this.label68.Text = "Serial Number:";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 68);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(57, 13);
            this.label69.TabIndex = 1;
            this.label69.Text = "Diameters:";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 16);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(48, 13);
            this.label70.TabIndex = 0;
            this.label70.Text = "Address:";
            // 
            // grpDx04
            // 
            this.grpDx04.Controls.Add(this.lblDx04Manuf);
            this.grpDx04.Controls.Add(this.lblDx04Code);
            this.grpDx04.Controls.Add(this.lblDx04Sn);
            this.grpDx04.Controls.Add(this.lblDx04Diam);
            this.grpDx04.Controls.Add(this.lblDx04Address);
            this.grpDx04.Controls.Add(this.label76);
            this.grpDx04.Controls.Add(this.label77);
            this.grpDx04.Controls.Add(this.label78);
            this.grpDx04.Controls.Add(this.label79);
            this.grpDx04.Controls.Add(this.label80);
            this.grpDx04.ForeColor = System.Drawing.Color.White;
            this.grpDx04.Location = new System.Drawing.Point(6, 118);
            this.grpDx04.Name = "grpDx04";
            this.grpDx04.Size = new System.Drawing.Size(166, 92);
            this.grpDx04.TabIndex = 12;
            this.grpDx04.TabStop = false;
            this.grpDx04.Text = "Hopper ID:";
            // 
            // lblDx04Manuf
            // 
            this.lblDx04Manuf.AutoSize = true;
            this.lblDx04Manuf.Location = new System.Drawing.Point(84, 29);
            this.lblDx04Manuf.Name = "lblDx04Manuf";
            this.lblDx04Manuf.Size = new System.Drawing.Size(13, 13);
            this.lblDx04Manuf.TabIndex = 9;
            this.lblDx04Manuf.Text = "--";
            // 
            // lblDx04Code
            // 
            this.lblDx04Code.AutoSize = true;
            this.lblDx04Code.Location = new System.Drawing.Point(84, 42);
            this.lblDx04Code.Name = "lblDx04Code";
            this.lblDx04Code.Size = new System.Drawing.Size(13, 13);
            this.lblDx04Code.TabIndex = 8;
            this.lblDx04Code.Text = "--";
            // 
            // lblDx04Sn
            // 
            this.lblDx04Sn.AutoSize = true;
            this.lblDx04Sn.Location = new System.Drawing.Point(84, 55);
            this.lblDx04Sn.Name = "lblDx04Sn";
            this.lblDx04Sn.Size = new System.Drawing.Size(13, 13);
            this.lblDx04Sn.TabIndex = 7;
            this.lblDx04Sn.Text = "--";
            // 
            // lblDx04Diam
            // 
            this.lblDx04Diam.AutoSize = true;
            this.lblDx04Diam.Location = new System.Drawing.Point(84, 68);
            this.lblDx04Diam.Name = "lblDx04Diam";
            this.lblDx04Diam.Size = new System.Drawing.Size(13, 13);
            this.lblDx04Diam.TabIndex = 6;
            this.lblDx04Diam.Text = "--";
            // 
            // lblDx04Address
            // 
            this.lblDx04Address.AutoSize = true;
            this.lblDx04Address.Location = new System.Drawing.Point(84, 16);
            this.lblDx04Address.Name = "lblDx04Address";
            this.lblDx04Address.Size = new System.Drawing.Size(13, 13);
            this.lblDx04Address.TabIndex = 5;
            this.lblDx04Address.Text = "--";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 29);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(73, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "Manufacturer:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(6, 42);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(75, 13);
            this.label77.TabIndex = 3;
            this.label77.Text = "Product Code:";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 55);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(76, 13);
            this.label78.TabIndex = 2;
            this.label78.Text = "Serial Number:";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(6, 68);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(57, 13);
            this.label79.TabIndex = 1;
            this.label79.Text = "Diameters:";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(6, 16);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(48, 13);
            this.label80.TabIndex = 0;
            this.label80.Text = "Address:";
            // 
            // grpDx03
            // 
            this.grpDx03.Controls.Add(this.lblDx03Manuf);
            this.grpDx03.Controls.Add(this.lblDx03Code);
            this.grpDx03.Controls.Add(this.lblDx03Sn);
            this.grpDx03.Controls.Add(this.lblDx03Diam);
            this.grpDx03.Controls.Add(this.lblDx03Address);
            this.grpDx03.Controls.Add(this.label86);
            this.grpDx03.Controls.Add(this.label87);
            this.grpDx03.Controls.Add(this.label88);
            this.grpDx03.Controls.Add(this.label89);
            this.grpDx03.Controls.Add(this.labelxx);
            this.grpDx03.ForeColor = System.Drawing.Color.White;
            this.grpDx03.Location = new System.Drawing.Point(6, 20);
            this.grpDx03.Name = "grpDx03";
            this.grpDx03.Size = new System.Drawing.Size(166, 92);
            this.grpDx03.TabIndex = 11;
            this.grpDx03.TabStop = false;
            this.grpDx03.Text = "Hopper ID:";
            // 
            // lblDx03Manuf
            // 
            this.lblDx03Manuf.AutoSize = true;
            this.lblDx03Manuf.Location = new System.Drawing.Point(84, 29);
            this.lblDx03Manuf.Name = "lblDx03Manuf";
            this.lblDx03Manuf.Size = new System.Drawing.Size(13, 13);
            this.lblDx03Manuf.TabIndex = 9;
            this.lblDx03Manuf.Text = "--";
            // 
            // lblDx03Code
            // 
            this.lblDx03Code.AutoSize = true;
            this.lblDx03Code.Location = new System.Drawing.Point(84, 42);
            this.lblDx03Code.Name = "lblDx03Code";
            this.lblDx03Code.Size = new System.Drawing.Size(13, 13);
            this.lblDx03Code.TabIndex = 8;
            this.lblDx03Code.Text = "--";
            // 
            // lblDx03Sn
            // 
            this.lblDx03Sn.AutoSize = true;
            this.lblDx03Sn.Location = new System.Drawing.Point(84, 55);
            this.lblDx03Sn.Name = "lblDx03Sn";
            this.lblDx03Sn.Size = new System.Drawing.Size(13, 13);
            this.lblDx03Sn.TabIndex = 7;
            this.lblDx03Sn.Text = "--";
            // 
            // lblDx03Diam
            // 
            this.lblDx03Diam.AutoSize = true;
            this.lblDx03Diam.Location = new System.Drawing.Point(84, 68);
            this.lblDx03Diam.Name = "lblDx03Diam";
            this.lblDx03Diam.Size = new System.Drawing.Size(13, 13);
            this.lblDx03Diam.TabIndex = 6;
            this.lblDx03Diam.Text = "--";
            // 
            // lblDx03Address
            // 
            this.lblDx03Address.AutoSize = true;
            this.lblDx03Address.Location = new System.Drawing.Point(84, 16);
            this.lblDx03Address.Name = "lblDx03Address";
            this.lblDx03Address.Size = new System.Drawing.Size(13, 13);
            this.lblDx03Address.TabIndex = 5;
            this.lblDx03Address.Text = "--";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(6, 29);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(73, 13);
            this.label86.TabIndex = 4;
            this.label86.Text = "Manufacturer:";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(6, 42);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(75, 13);
            this.label87.TabIndex = 3;
            this.label87.Text = "Product Code:";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(6, 55);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(76, 13);
            this.label88.TabIndex = 2;
            this.label88.Text = "Serial Number:";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(6, 68);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(57, 13);
            this.label89.TabIndex = 1;
            this.label89.Text = "Diameters:";
            // 
            // labelxx
            // 
            this.labelxx.AutoSize = true;
            this.labelxx.Location = new System.Drawing.Point(6, 16);
            this.labelxx.Name = "labelxx";
            this.labelxx.Size = new System.Drawing.Size(48, 13);
            this.labelxx.TabIndex = 0;
            this.labelxx.Text = "Address:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.grpSx06);
            this.groupBox2.Controls.Add(this.grpSx05);
            this.groupBox2.Controls.Add(this.grpSx04);
            this.groupBox2.Controls.Add(this.grpSx03);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(229, 61);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(180, 413);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hopper SX";
            // 
            // grpSx06
            // 
            this.grpSx06.Controls.Add(this.lblSx06Manuf);
            this.grpSx06.Controls.Add(this.lblSx06Code);
            this.grpSx06.Controls.Add(this.lblSx06Sn);
            this.grpSx06.Controls.Add(this.lblSx06Diam);
            this.grpSx06.Controls.Add(this.lblSx06Address);
            this.grpSx06.Controls.Add(this.label46);
            this.grpSx06.Controls.Add(this.label47);
            this.grpSx06.Controls.Add(this.label48);
            this.grpSx06.Controls.Add(this.label49);
            this.grpSx06.Controls.Add(this.label50);
            this.grpSx06.ForeColor = System.Drawing.Color.White;
            this.grpSx06.Location = new System.Drawing.Point(6, 314);
            this.grpSx06.Name = "grpSx06";
            this.grpSx06.Size = new System.Drawing.Size(166, 92);
            this.grpSx06.TabIndex = 10;
            this.grpSx06.TabStop = false;
            this.grpSx06.Text = "Hopper ID:";
            // 
            // lblSx06Manuf
            // 
            this.lblSx06Manuf.AutoSize = true;
            this.lblSx06Manuf.Location = new System.Drawing.Point(84, 29);
            this.lblSx06Manuf.Name = "lblSx06Manuf";
            this.lblSx06Manuf.Size = new System.Drawing.Size(13, 13);
            this.lblSx06Manuf.TabIndex = 9;
            this.lblSx06Manuf.Text = "--";
            // 
            // lblSx06Code
            // 
            this.lblSx06Code.AutoSize = true;
            this.lblSx06Code.Location = new System.Drawing.Point(84, 42);
            this.lblSx06Code.Name = "lblSx06Code";
            this.lblSx06Code.Size = new System.Drawing.Size(13, 13);
            this.lblSx06Code.TabIndex = 8;
            this.lblSx06Code.Text = "--";
            // 
            // lblSx06Sn
            // 
            this.lblSx06Sn.AutoSize = true;
            this.lblSx06Sn.Location = new System.Drawing.Point(84, 55);
            this.lblSx06Sn.Name = "lblSx06Sn";
            this.lblSx06Sn.Size = new System.Drawing.Size(13, 13);
            this.lblSx06Sn.TabIndex = 7;
            this.lblSx06Sn.Text = "--";
            // 
            // lblSx06Diam
            // 
            this.lblSx06Diam.AutoSize = true;
            this.lblSx06Diam.Location = new System.Drawing.Point(84, 68);
            this.lblSx06Diam.Name = "lblSx06Diam";
            this.lblSx06Diam.Size = new System.Drawing.Size(13, 13);
            this.lblSx06Diam.TabIndex = 6;
            this.lblSx06Diam.Text = "--";
            // 
            // lblSx06Address
            // 
            this.lblSx06Address.AutoSize = true;
            this.lblSx06Address.Location = new System.Drawing.Point(84, 16);
            this.lblSx06Address.Name = "lblSx06Address";
            this.lblSx06Address.Size = new System.Drawing.Size(13, 13);
            this.lblSx06Address.TabIndex = 5;
            this.lblSx06Address.Text = "--";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 29);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(73, 13);
            this.label46.TabIndex = 4;
            this.label46.Text = "Manufacturer:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 42);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(75, 13);
            this.label47.TabIndex = 3;
            this.label47.Text = "Product Code:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 55);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(76, 13);
            this.label48.TabIndex = 2;
            this.label48.Text = "Serial Number:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 68);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(57, 13);
            this.label49.TabIndex = 1;
            this.label49.Text = "Diameters:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 16);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(48, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Address:";
            // 
            // grpSx05
            // 
            this.grpSx05.Controls.Add(this.lblSx05Manuf);
            this.grpSx05.Controls.Add(this.lblSx05Code);
            this.grpSx05.Controls.Add(this.lblSx05Sn);
            this.grpSx05.Controls.Add(this.lblSx05Diam);
            this.grpSx05.Controls.Add(this.lblSx05Address);
            this.grpSx05.Controls.Add(this.label36);
            this.grpSx05.Controls.Add(this.label37);
            this.grpSx05.Controls.Add(this.label38);
            this.grpSx05.Controls.Add(this.label39);
            this.grpSx05.Controls.Add(this.label40);
            this.grpSx05.ForeColor = System.Drawing.Color.White;
            this.grpSx05.Location = new System.Drawing.Point(6, 216);
            this.grpSx05.Name = "grpSx05";
            this.grpSx05.Size = new System.Drawing.Size(166, 92);
            this.grpSx05.TabIndex = 8;
            this.grpSx05.TabStop = false;
            this.grpSx05.Text = "Hopper ID:";
            // 
            // lblSx05Manuf
            // 
            this.lblSx05Manuf.AutoSize = true;
            this.lblSx05Manuf.Location = new System.Drawing.Point(84, 29);
            this.lblSx05Manuf.Name = "lblSx05Manuf";
            this.lblSx05Manuf.Size = new System.Drawing.Size(13, 13);
            this.lblSx05Manuf.TabIndex = 9;
            this.lblSx05Manuf.Text = "--";
            // 
            // lblSx05Code
            // 
            this.lblSx05Code.AutoSize = true;
            this.lblSx05Code.Location = new System.Drawing.Point(84, 42);
            this.lblSx05Code.Name = "lblSx05Code";
            this.lblSx05Code.Size = new System.Drawing.Size(13, 13);
            this.lblSx05Code.TabIndex = 8;
            this.lblSx05Code.Text = "--";
            // 
            // lblSx05Sn
            // 
            this.lblSx05Sn.AutoSize = true;
            this.lblSx05Sn.Location = new System.Drawing.Point(84, 55);
            this.lblSx05Sn.Name = "lblSx05Sn";
            this.lblSx05Sn.Size = new System.Drawing.Size(13, 13);
            this.lblSx05Sn.TabIndex = 7;
            this.lblSx05Sn.Text = "--";
            // 
            // lblSx05Diam
            // 
            this.lblSx05Diam.AutoSize = true;
            this.lblSx05Diam.Location = new System.Drawing.Point(84, 68);
            this.lblSx05Diam.Name = "lblSx05Diam";
            this.lblSx05Diam.Size = new System.Drawing.Size(13, 13);
            this.lblSx05Diam.TabIndex = 6;
            this.lblSx05Diam.Text = "--";
            // 
            // lblSx05Address
            // 
            this.lblSx05Address.AutoSize = true;
            this.lblSx05Address.Location = new System.Drawing.Point(84, 16);
            this.lblSx05Address.Name = "lblSx05Address";
            this.lblSx05Address.Size = new System.Drawing.Size(13, 13);
            this.lblSx05Address.TabIndex = 5;
            this.lblSx05Address.Text = "--";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 29);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(73, 13);
            this.label36.TabIndex = 4;
            this.label36.Text = "Manufacturer:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 42);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 13);
            this.label37.TabIndex = 3;
            this.label37.Text = "Product Code:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 55);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(76, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Serial Number:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 68);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(57, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "Diameters:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 16);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(48, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Address:";
            // 
            // grpSx04
            // 
            this.grpSx04.Controls.Add(this.lblSx04Manuf);
            this.grpSx04.Controls.Add(this.lblSx04Code);
            this.grpSx04.Controls.Add(this.lblSx04Sn);
            this.grpSx04.Controls.Add(this.lblSx04Diam);
            this.grpSx04.Controls.Add(this.lblSx04Address);
            this.grpSx04.Controls.Add(this.label26);
            this.grpSx04.Controls.Add(this.label27);
            this.grpSx04.Controls.Add(this.label28);
            this.grpSx04.Controls.Add(this.label29);
            this.grpSx04.Controls.Add(this.label30);
            this.grpSx04.ForeColor = System.Drawing.Color.White;
            this.grpSx04.Location = new System.Drawing.Point(6, 118);
            this.grpSx04.Name = "grpSx04";
            this.grpSx04.Size = new System.Drawing.Size(166, 92);
            this.grpSx04.TabIndex = 7;
            this.grpSx04.TabStop = false;
            this.grpSx04.Text = "Hopper ID:";
            // 
            // lblSx04Manuf
            // 
            this.lblSx04Manuf.AutoSize = true;
            this.lblSx04Manuf.Location = new System.Drawing.Point(84, 29);
            this.lblSx04Manuf.Name = "lblSx04Manuf";
            this.lblSx04Manuf.Size = new System.Drawing.Size(13, 13);
            this.lblSx04Manuf.TabIndex = 9;
            this.lblSx04Manuf.Text = "--";
            // 
            // lblSx04Code
            // 
            this.lblSx04Code.AutoSize = true;
            this.lblSx04Code.Location = new System.Drawing.Point(84, 42);
            this.lblSx04Code.Name = "lblSx04Code";
            this.lblSx04Code.Size = new System.Drawing.Size(13, 13);
            this.lblSx04Code.TabIndex = 8;
            this.lblSx04Code.Text = "--";
            // 
            // lblSx04Sn
            // 
            this.lblSx04Sn.AutoSize = true;
            this.lblSx04Sn.Location = new System.Drawing.Point(84, 55);
            this.lblSx04Sn.Name = "lblSx04Sn";
            this.lblSx04Sn.Size = new System.Drawing.Size(13, 13);
            this.lblSx04Sn.TabIndex = 7;
            this.lblSx04Sn.Text = "--";
            // 
            // lblSx04Diam
            // 
            this.lblSx04Diam.AutoSize = true;
            this.lblSx04Diam.Location = new System.Drawing.Point(84, 68);
            this.lblSx04Diam.Name = "lblSx04Diam";
            this.lblSx04Diam.Size = new System.Drawing.Size(13, 13);
            this.lblSx04Diam.TabIndex = 6;
            this.lblSx04Diam.Text = "--";
            // 
            // lblSx04Address
            // 
            this.lblSx04Address.AutoSize = true;
            this.lblSx04Address.Location = new System.Drawing.Point(84, 16);
            this.lblSx04Address.Name = "lblSx04Address";
            this.lblSx04Address.Size = new System.Drawing.Size(13, 13);
            this.lblSx04Address.TabIndex = 5;
            this.lblSx04Address.Text = "--";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 29);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 13);
            this.label26.TabIndex = 4;
            this.label26.Text = "Manufacturer:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 42);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "Product Code:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 55);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "Serial Number:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 68);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(57, 13);
            this.label29.TabIndex = 1;
            this.label29.Text = "Diameters:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 16);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(48, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Address:";
            // 
            // grpSx03
            // 
            this.grpSx03.Controls.Add(this.lblSx03Manuf);
            this.grpSx03.Controls.Add(this.lblSx03Code);
            this.grpSx03.Controls.Add(this.lblSx03Sn);
            this.grpSx03.Controls.Add(this.lblSx03Diam);
            this.grpSx03.Controls.Add(this.lblSx03Address);
            this.grpSx03.Controls.Add(this.label16);
            this.grpSx03.Controls.Add(this.label17);
            this.grpSx03.Controls.Add(this.label18);
            this.grpSx03.Controls.Add(this.label19);
            this.grpSx03.Controls.Add(this.label20);
            this.grpSx03.ForeColor = System.Drawing.Color.White;
            this.grpSx03.Location = new System.Drawing.Point(6, 20);
            this.grpSx03.Name = "grpSx03";
            this.grpSx03.Size = new System.Drawing.Size(166, 92);
            this.grpSx03.TabIndex = 6;
            this.grpSx03.TabStop = false;
            this.grpSx03.Text = "Hopper ID:";
            // 
            // lblSx03Manuf
            // 
            this.lblSx03Manuf.AutoSize = true;
            this.lblSx03Manuf.Location = new System.Drawing.Point(84, 29);
            this.lblSx03Manuf.Name = "lblSx03Manuf";
            this.lblSx03Manuf.Size = new System.Drawing.Size(13, 13);
            this.lblSx03Manuf.TabIndex = 9;
            this.lblSx03Manuf.Text = "--";
            // 
            // lblSx03Code
            // 
            this.lblSx03Code.AutoSize = true;
            this.lblSx03Code.Location = new System.Drawing.Point(84, 42);
            this.lblSx03Code.Name = "lblSx03Code";
            this.lblSx03Code.Size = new System.Drawing.Size(13, 13);
            this.lblSx03Code.TabIndex = 8;
            this.lblSx03Code.Text = "--";
            // 
            // lblSx03Sn
            // 
            this.lblSx03Sn.AutoSize = true;
            this.lblSx03Sn.Location = new System.Drawing.Point(84, 55);
            this.lblSx03Sn.Name = "lblSx03Sn";
            this.lblSx03Sn.Size = new System.Drawing.Size(13, 13);
            this.lblSx03Sn.TabIndex = 7;
            this.lblSx03Sn.Text = "--";
            // 
            // lblSx03Diam
            // 
            this.lblSx03Diam.AutoSize = true;
            this.lblSx03Diam.Location = new System.Drawing.Point(84, 68);
            this.lblSx03Diam.Name = "lblSx03Diam";
            this.lblSx03Diam.Size = new System.Drawing.Size(13, 13);
            this.lblSx03Diam.TabIndex = 6;
            this.lblSx03Diam.Text = "--";
            // 
            // lblSx03Address
            // 
            this.lblSx03Address.AutoSize = true;
            this.lblSx03Address.Location = new System.Drawing.Point(84, 16);
            this.lblSx03Address.Name = "lblSx03Address";
            this.lblSx03Address.Size = new System.Drawing.Size(13, 13);
            this.lblSx03Address.TabIndex = 5;
            this.lblSx03Address.Text = "--";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Manufacturer:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 42);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Product Code:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Serial Number:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 68);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Diameters:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Address:";
            // 
            // lstComScopeSx
            // 
            this.lstComScopeSx.FormattingEnabled = true;
            this.lstComScopeSx.Location = new System.Drawing.Point(12, 67);
            this.lstComScopeSx.Name = "lstComScopeSx";
            this.lstComScopeSx.Size = new System.Drawing.Size(211, 407);
            this.lstComScopeSx.TabIndex = 2;
            // 
            // lstComScopeDx
            // 
            this.lstComScopeDx.FormattingEnabled = true;
            this.lstComScopeDx.Location = new System.Drawing.Point(601, 67);
            this.lstComScopeDx.Name = "lstComScopeDx";
            this.lstComScopeDx.Size = new System.Drawing.Size(211, 407);
            this.lstComScopeDx.TabIndex = 3;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(229, 10);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(366, 39);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Hoppers Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(229, 480);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 39);
            this.button1.TabIndex = 5;
            this.button1.Text = "Empty SX hopper";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(415, 480);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(180, 39);
            this.button2.TabIndex = 6;
            this.button2.Text = "Empty DX hopper";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmHoppers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.ClientSize = new System.Drawing.Size(819, 605);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lstComScopeDx);
            this.Controls.Add(this.lstComScopeSx);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmHoppers";
            this.Text = " ";
            this.Load += new System.EventHandler(this.frmHoppers_Load);
            this.groupBox1.ResumeLayout(false);
            this.grpDx06.ResumeLayout(false);
            this.grpDx06.PerformLayout();
            this.grpDx05.ResumeLayout(false);
            this.grpDx05.PerformLayout();
            this.grpDx04.ResumeLayout(false);
            this.grpDx04.PerformLayout();
            this.grpDx03.ResumeLayout(false);
            this.grpDx03.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.grpSx06.ResumeLayout(false);
            this.grpSx06.PerformLayout();
            this.grpSx05.ResumeLayout(false);
            this.grpSx05.PerformLayout();
            this.grpSx04.ResumeLayout(false);
            this.grpSx04.PerformLayout();
            this.grpSx03.ResumeLayout(false);
            this.grpSx03.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox lstComScopeSx;
        private System.Windows.Forms.ListBox lstComScopeDx;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox grpDx06;
        private System.Windows.Forms.Label lblDx06Manuf;
        private System.Windows.Forms.Label lblDx06Code;
        private System.Windows.Forms.Label lblDx06Sn;
        private System.Windows.Forms.Label lblDx06Diam;
        private System.Windows.Forms.Label lblDx06Address;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.GroupBox grpDx05;
        private System.Windows.Forms.Label lblDx05Manuf;
        private System.Windows.Forms.Label lblDx05Code;
        private System.Windows.Forms.Label lblDx05Sn;
        private System.Windows.Forms.Label lblDx05Diam;
        private System.Windows.Forms.Label lblDx05Address;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.GroupBox grpDx04;
        private System.Windows.Forms.Label lblDx04Manuf;
        private System.Windows.Forms.Label lblDx04Code;
        private System.Windows.Forms.Label lblDx04Sn;
        private System.Windows.Forms.Label lblDx04Diam;
        private System.Windows.Forms.Label lblDx04Address;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.GroupBox grpDx03;
        private System.Windows.Forms.Label lblDx03Manuf;
        private System.Windows.Forms.Label lblDx03Code;
        private System.Windows.Forms.Label lblDx03Sn;
        private System.Windows.Forms.Label lblDx03Diam;
        private System.Windows.Forms.Label lblDx03Address;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label labelxx;
        private System.Windows.Forms.GroupBox grpSx06;
        private System.Windows.Forms.Label lblSx06Manuf;
        private System.Windows.Forms.Label lblSx06Code;
        private System.Windows.Forms.Label lblSx06Sn;
        private System.Windows.Forms.Label lblSx06Diam;
        private System.Windows.Forms.Label lblSx06Address;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox grpSx05;
        private System.Windows.Forms.Label lblSx05Manuf;
        private System.Windows.Forms.Label lblSx05Code;
        private System.Windows.Forms.Label lblSx05Sn;
        private System.Windows.Forms.Label lblSx05Diam;
        private System.Windows.Forms.Label lblSx05Address;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox grpSx04;
        private System.Windows.Forms.Label lblSx04Manuf;
        private System.Windows.Forms.Label lblSx04Code;
        private System.Windows.Forms.Label lblSx04Sn;
        private System.Windows.Forms.Label lblSx04Diam;
        private System.Windows.Forms.Label lblSx04Address;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox grpSx03;
        private System.Windows.Forms.Label lblSx03Manuf;
        private System.Windows.Forms.Label lblSx03Code;
        private System.Windows.Forms.Label lblSx03Sn;
        private System.Windows.Forms.Label lblSx03Diam;
        private System.Windows.Forms.Label lblSx03Address;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}